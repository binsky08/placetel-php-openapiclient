<?php
/**
 * PutSipUsersTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Placetel API
 *
 * --- # Introduction  <h2>Create custom integrations or automate your workflows with the Placetel REST API.</h2>  To make a request to our API, you will need to specify an HTTP **method** and a **path**. Additionally, you can specify request **headers**, **query** and **body** parameters. The API will return the response status code, response headers, and dependening on status and resource a response body.  The documentation for every operation displays example requests and responses to provide you the best understanding of our API.  # Pagination  Be aware, that requests which return multiple resources will be paginated by default. You can specify further pages with the `page` parameter. For some resources, you can also set a custom page size (usually up to 100) with the `per_page` parameter. Note that for technical reasons not all endpoints respect the `per_page` parameter.
 *
 * The version of the OpenAPI document: 2.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.3.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace OpenAPI\Client\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * PutSipUsersTest Class Doc Comment
 *
 * @category    Class
 * @description Update a sip user
 * @package     OpenAPI\Client
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class PutSipUsersTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "PutSipUsers"
     */
    public function testPutSipUsers()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "did"
     */
    public function testPropertyDid()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "callerid"
     */
    public function testPropertyCallerid()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "webuser_id"
     */
    public function testPropertyWebuserId()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "contact_speeddialing"
     */
    public function testPropertyContactSpeeddialing()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "p_asserted_identity"
     */
    public function testPropertyPAssertedIdentity()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "automatic_prefix"
     */
    public function testPropertyAutomaticPrefix()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "blocked_prefixes"
     */
    public function testPropertyBlockedPrefixes()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "routing_plan_id"
     */
    public function testPropertyRoutingPlanId()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "hotdesk_login"
     */
    public function testPropertyHotdeskLogin()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "hotdesk_pin"
     */
    public function testPropertyHotdeskPin()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "hotdesk_type"
     */
    public function testPropertyHotdeskType()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "hotdesk_provisioning_id"
     */
    public function testPropertyHotdeskProvisioningId()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
