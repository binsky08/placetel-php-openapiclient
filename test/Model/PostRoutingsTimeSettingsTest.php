<?php
/**
 * PostRoutingsTimeSettingsTest
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Placetel API
 *
 * --- # Introduction  <h2>Create custom integrations or automate your workflows with the Placetel REST API.</h2>  To make a request to our API, you will need to specify an HTTP **method** and a **path**. Additionally, you can specify request **headers**, **query** and **body** parameters. The API will return the response status code, response headers, and dependening on status and resource a response body.  The documentation for every operation displays example requests and responses to provide you the best understanding of our API.  # Pagination  Be aware, that requests which return multiple resources will be paginated by default. You can specify further pages with the `page` parameter. For some resources, you can also set a custom page size (usually up to 100) with the `per_page` parameter. Note that for technical reasons not all endpoints respect the `per_page` parameter.
 *
 * The version of the OpenAPI document: 2.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.3.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace OpenAPI\Client\Test\Model;

use PHPUnit\Framework\TestCase;

/**
 * PostRoutingsTimeSettingsTest Class Doc Comment
 *
 * @category    Class
 * @description PostRoutingsTimeSettings
 * @package     OpenAPI\Client
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class PostRoutingsTimeSettingsTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test "PostRoutingsTimeSettings"
     */
    public function testPostRoutingsTimeSettings()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "enabled"
     */
    public function testPropertyEnabled()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "mon"
     */
    public function testPropertyMon()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "tue"
     */
    public function testPropertyTue()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "wed"
     */
    public function testPropertyWed()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "thu"
     */
    public function testPropertyThu()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "fri"
     */
    public function testPropertyFri()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "sat"
     */
    public function testPropertySat()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "sun"
     */
    public function testPropertySun()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "extended_time_settings"
     */
    public function testPropertyExtendedTimeSettings()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "time_from"
     */
    public function testPropertyTimeFrom()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "time_to"
     */
    public function testPropertyTimeTo()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "additional_dates"
     */
    public function testPropertyAdditionalDates()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test attribute "time_ranges"
     */
    public function testPropertyTimeRanges()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
