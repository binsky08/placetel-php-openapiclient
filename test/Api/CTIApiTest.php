<?php
/**
 * CTIApiTest
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Placetel API
 *
 * --- # Introduction  <h2>Create custom integrations or automate your workflows with the Placetel REST API.</h2>  To make a request to our API, you will need to specify an HTTP **method** and a **path**. Additionally, you can specify request **headers**, **query** and **body** parameters. The API will return the response status code, response headers, and dependening on status and resource a response body.  The documentation for every operation displays example requests and responses to provide you the best understanding of our API.  # Pagination  Be aware, that requests which return multiple resources will be paginated by default. You can specify further pages with the `page` parameter. For some resources, you can also set a custom page size (usually up to 100) with the `per_page` parameter. Note that for technical reasons not all endpoints respect the `per_page` parameter.
 *
 * The version of the OpenAPI document: 2.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.3.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace OpenAPI\Client\Test\Api;

use \OpenAPI\Client\Configuration;
use \OpenAPI\Client\ApiException;
use \OpenAPI\Client\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * CTIApiTest Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class CTIApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for getCtiMac
     *
     * Get config params.
     *
     */
    public function testGetCtiMac()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacAnswer
     *
     * Answer.
     *
     */
    public function testPostCtiMacAnswer()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacBlindTransfer
     *
     * Blind transfer.
     *
     */
    public function testPostCtiMacBlindTransfer()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacCompleteConference
     *
     * Complete conference.
     *
     */
    public function testPostCtiMacCompleteConference()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacCompleteTransfer
     *
     * Complete transfer.
     *
     */
    public function testPostCtiMacCompleteTransfer()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacDecline
     *
     * Decline.
     *
     */
    public function testPostCtiMacDecline()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacDial
     *
     * Dial.
     *
     */
    public function testPostCtiMacDial()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacDialDigit
     *
     * Dial digit.
     *
     */
    public function testPostCtiMacDialDigit()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacHangup
     *
     * Hangup.
     *
     */
    public function testPostCtiMacHangup()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacHold
     *
     * Hold.
     *
     */
    public function testPostCtiMacHold()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacResume
     *
     * Resume.
     *
     */
    public function testPostCtiMacResume()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacSendDtmfDigits
     *
     * Send DTMF Digits.
     *
     */
    public function testPostCtiMacSendDtmfDigits()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacStartConference
     *
     * Start conference.
     *
     */
    public function testPostCtiMacStartConference()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for postCtiMacStartTransfer
     *
     * Start transfer.
     *
     */
    public function testPostCtiMacStartTransfer()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for putCtiMac
     *
     * Set config params.
     *
     */
    public function testPutCtiMac()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
