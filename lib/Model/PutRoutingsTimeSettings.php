<?php
/**
 * PutRoutingsTimeSettings
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Placetel API
 *
 * --- # Introduction  <h2>Create custom integrations or automate your workflows with the Placetel REST API.</h2>  To make a request to our API, you will need to specify an HTTP **method** and a **path**. Additionally, you can specify request **headers**, **query** and **body** parameters. The API will return the response status code, response headers, and dependening on status and resource a response body.  The documentation for every operation displays example requests and responses to provide you the best understanding of our API.  # Pagination  Be aware, that requests which return multiple resources will be paginated by default. You can specify further pages with the `page` parameter. For some resources, you can also set a custom page size (usually up to 100) with the `per_page` parameter. Note that for technical reasons not all endpoints respect the `per_page` parameter.
 *
 * The version of the OpenAPI document: 2.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.3.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;

use \ArrayAccess;
use \OpenAPI\Client\ObjectSerializer;

/**
 * PutRoutingsTimeSettings Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class PutRoutingsTimeSettings implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'putRoutings_time_settings';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'enabled' => 'bool',
        'mon' => 'bool',
        'tue' => 'bool',
        'wed' => 'bool',
        'thu' => 'bool',
        'fri' => 'bool',
        'sat' => 'bool',
        'sun' => 'bool',
        'extended_time_settings' => 'bool',
        'time_from' => 'string',
        'time_to' => 'string',
        'additional_dates' => 'string',
        'time_ranges' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'enabled' => null,
        'mon' => null,
        'tue' => null,
        'wed' => null,
        'thu' => null,
        'fri' => null,
        'sat' => null,
        'sun' => null,
        'extended_time_settings' => null,
        'time_from' => null,
        'time_to' => null,
        'additional_dates' => null,
        'time_ranges' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'enabled' => false,
        'mon' => false,
        'tue' => false,
        'wed' => false,
        'thu' => false,
        'fri' => false,
        'sat' => false,
        'sun' => false,
        'extended_time_settings' => false,
        'time_from' => false,
        'time_to' => false,
        'additional_dates' => false,
        'time_ranges' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'enabled' => 'enabled',
        'mon' => 'mon',
        'tue' => 'tue',
        'wed' => 'wed',
        'thu' => 'thu',
        'fri' => 'fri',
        'sat' => 'sat',
        'sun' => 'sun',
        'extended_time_settings' => 'extended_time_settings',
        'time_from' => 'time_from',
        'time_to' => 'time_to',
        'additional_dates' => 'additional_dates',
        'time_ranges' => 'time_ranges'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'enabled' => 'setEnabled',
        'mon' => 'setMon',
        'tue' => 'setTue',
        'wed' => 'setWed',
        'thu' => 'setThu',
        'fri' => 'setFri',
        'sat' => 'setSat',
        'sun' => 'setSun',
        'extended_time_settings' => 'setExtendedTimeSettings',
        'time_from' => 'setTimeFrom',
        'time_to' => 'setTimeTo',
        'additional_dates' => 'setAdditionalDates',
        'time_ranges' => 'setTimeRanges'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'enabled' => 'getEnabled',
        'mon' => 'getMon',
        'tue' => 'getTue',
        'wed' => 'getWed',
        'thu' => 'getThu',
        'fri' => 'getFri',
        'sat' => 'getSat',
        'sun' => 'getSun',
        'extended_time_settings' => 'getExtendedTimeSettings',
        'time_from' => 'getTimeFrom',
        'time_to' => 'getTimeTo',
        'additional_dates' => 'getAdditionalDates',
        'time_ranges' => 'getTimeRanges'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('enabled', $data ?? [], null);
        $this->setIfExists('mon', $data ?? [], null);
        $this->setIfExists('tue', $data ?? [], null);
        $this->setIfExists('wed', $data ?? [], null);
        $this->setIfExists('thu', $data ?? [], null);
        $this->setIfExists('fri', $data ?? [], null);
        $this->setIfExists('sat', $data ?? [], null);
        $this->setIfExists('sun', $data ?? [], null);
        $this->setIfExists('extended_time_settings', $data ?? [], null);
        $this->setIfExists('time_from', $data ?? [], null);
        $this->setIfExists('time_to', $data ?? [], null);
        $this->setIfExists('additional_dates', $data ?? [], null);
        $this->setIfExists('time_ranges', $data ?? [], null);
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets enabled
     *
     * @return bool|null
     */
    public function getEnabled()
    {
        return $this->container['enabled'];
    }

    /**
     * Sets enabled
     *
     * @param bool|null $enabled enabled
     *
     * @return self
     */
    public function setEnabled($enabled)
    {
        if (is_null($enabled)) {
            throw new \InvalidArgumentException('non-nullable enabled cannot be null');
        }
        $this->container['enabled'] = $enabled;

        return $this;
    }

    /**
     * Gets mon
     *
     * @return bool|null
     */
    public function getMon()
    {
        return $this->container['mon'];
    }

    /**
     * Sets mon
     *
     * @param bool|null $mon Activates routing objects for mondays
     *
     * @return self
     */
    public function setMon($mon)
    {
        if (is_null($mon)) {
            throw new \InvalidArgumentException('non-nullable mon cannot be null');
        }
        $this->container['mon'] = $mon;

        return $this;
    }

    /**
     * Gets tue
     *
     * @return bool|null
     */
    public function getTue()
    {
        return $this->container['tue'];
    }

    /**
     * Sets tue
     *
     * @param bool|null $tue Activates routing objects for tuesdays
     *
     * @return self
     */
    public function setTue($tue)
    {
        if (is_null($tue)) {
            throw new \InvalidArgumentException('non-nullable tue cannot be null');
        }
        $this->container['tue'] = $tue;

        return $this;
    }

    /**
     * Gets wed
     *
     * @return bool|null
     */
    public function getWed()
    {
        return $this->container['wed'];
    }

    /**
     * Sets wed
     *
     * @param bool|null $wed Activates routing objects for wednesdays
     *
     * @return self
     */
    public function setWed($wed)
    {
        if (is_null($wed)) {
            throw new \InvalidArgumentException('non-nullable wed cannot be null');
        }
        $this->container['wed'] = $wed;

        return $this;
    }

    /**
     * Gets thu
     *
     * @return bool|null
     */
    public function getThu()
    {
        return $this->container['thu'];
    }

    /**
     * Sets thu
     *
     * @param bool|null $thu Activates routing objects for thursdays
     *
     * @return self
     */
    public function setThu($thu)
    {
        if (is_null($thu)) {
            throw new \InvalidArgumentException('non-nullable thu cannot be null');
        }
        $this->container['thu'] = $thu;

        return $this;
    }

    /**
     * Gets fri
     *
     * @return bool|null
     */
    public function getFri()
    {
        return $this->container['fri'];
    }

    /**
     * Sets fri
     *
     * @param bool|null $fri Activates routing objects for fridays
     *
     * @return self
     */
    public function setFri($fri)
    {
        if (is_null($fri)) {
            throw new \InvalidArgumentException('non-nullable fri cannot be null');
        }
        $this->container['fri'] = $fri;

        return $this;
    }

    /**
     * Gets sat
     *
     * @return bool|null
     */
    public function getSat()
    {
        return $this->container['sat'];
    }

    /**
     * Sets sat
     *
     * @param bool|null $sat Activates routing objects for saturdays
     *
     * @return self
     */
    public function setSat($sat)
    {
        if (is_null($sat)) {
            throw new \InvalidArgumentException('non-nullable sat cannot be null');
        }
        $this->container['sat'] = $sat;

        return $this;
    }

    /**
     * Gets sun
     *
     * @return bool|null
     */
    public function getSun()
    {
        return $this->container['sun'];
    }

    /**
     * Sets sun
     *
     * @param bool|null $sun Activates routing objects for sundays
     *
     * @return self
     */
    public function setSun($sun)
    {
        if (is_null($sun)) {
            throw new \InvalidArgumentException('non-nullable sun cannot be null');
        }
        $this->container['sun'] = $sun;

        return $this;
    }

    /**
     * Gets extended_time_settings
     *
     * @return bool|null
     */
    public function getExtendedTimeSettings()
    {
        return $this->container['extended_time_settings'];
    }

    /**
     * Sets extended_time_settings
     *
     * @param bool|null $extended_time_settings Activates extended time settings options.
     *
     * @return self
     */
    public function setExtendedTimeSettings($extended_time_settings)
    {
        if (is_null($extended_time_settings)) {
            throw new \InvalidArgumentException('non-nullable extended_time_settings cannot be null');
        }
        $this->container['extended_time_settings'] = $extended_time_settings;

        return $this;
    }

    /**
     * Gets time_from
     *
     * @return string|null
     */
    public function getTimeFrom()
    {
        return $this->container['time_from'];
    }

    /**
     * Sets time_from
     *
     * @param string|null $time_from Only used when `extended_time_settings` is `false`.<br> Example: `08:00:00`
     *
     * @return self
     */
    public function setTimeFrom($time_from)
    {
        if (is_null($time_from)) {
            throw new \InvalidArgumentException('non-nullable time_from cannot be null');
        }
        $this->container['time_from'] = $time_from;

        return $this;
    }

    /**
     * Gets time_to
     *
     * @return string|null
     */
    public function getTimeTo()
    {
        return $this->container['time_to'];
    }

    /**
     * Sets time_to
     *
     * @param string|null $time_to Only used when `extended_time_settings` is `false`.<br> Example: `19:59:59`
     *
     * @return self
     */
    public function setTimeTo($time_to)
    {
        if (is_null($time_to)) {
            throw new \InvalidArgumentException('non-nullable time_to cannot be null');
        }
        $this->container['time_to'] = $time_to;

        return $this;
    }

    /**
     * Gets additional_dates
     *
     * @return string|null
     */
    public function getAdditionalDates()
    {
        return $this->container['additional_dates'];
    }

    /**
     * Sets additional_dates
     *
     * @param string|null $additional_dates Additional dates w/o years to activate routing object for.<br> Only used when `extended_time_settings` is `true`.<br>Example: `9.9,2.1,24.12,25.12,31.12`
     *
     * @return self
     */
    public function setAdditionalDates($additional_dates)
    {
        if (is_null($additional_dates)) {
            throw new \InvalidArgumentException('non-nullable additional_dates cannot be null');
        }
        $this->container['additional_dates'] = $additional_dates;

        return $this;
    }

    /**
     * Gets time_ranges
     *
     * @return string|null
     */
    public function getTimeRanges()
    {
        return $this->container['time_ranges'];
    }

    /**
     * Sets time_ranges
     *
     * @param string|null $time_ranges Time ranges to activate routing object for.<br> Only used when `extended_time_settings` is `true`.<br>Example: `08:30:00-13:29:59,14:30:00-17:59:59`
     *
     * @return self
     */
    public function setTimeRanges($time_ranges)
    {
        if (is_null($time_ranges)) {
            throw new \InvalidArgumentException('non-nullable time_ranges cannot be null');
        }
        $this->container['time_ranges'] = $time_ranges;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


