<?php
/**
 * RoutingObjectGroup
 *
 * PHP version 7.4
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Placetel API
 *
 * --- # Introduction  <h2>Create custom integrations or automate your workflows with the Placetel REST API.</h2>  To make a request to our API, you will need to specify an HTTP **method** and a **path**. Additionally, you can specify request **headers**, **query** and **body** parameters. The API will return the response status code, response headers, and dependening on status and resource a response body.  The documentation for every operation displays example requests and responses to provide you the best understanding of our API.  # Pagination  Be aware, that requests which return multiple resources will be paginated by default. You can specify further pages with the `page` parameter. For some resources, you can also set a custom page size (usually up to 100) with the `per_page` parameter. Note that for technical reasons not all endpoints respect the `per_page` parameter.
 *
 * The version of the OpenAPI document: 2.0.0
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 7.3.0-SNAPSHOT
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;

use \ArrayAccess;
use \OpenAPI\Client\ObjectSerializer;

/**
 * RoutingObjectGroup Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<string, mixed>
 */
class RoutingObjectGroup implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'RoutingObject_group';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'id' => 'string',
        'group_ringing_time' => 'string',
        'backup_behaviour' => 'string',
        'backup_routing_object' => 'string',
        'backup_routing_object_id' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'id' => null,
        'group_ringing_time' => null,
        'backup_behaviour' => null,
        'backup_routing_object' => null,
        'backup_routing_object_id' => null
    ];

    /**
      * Array of nullable properties. Used for (de)serialization
      *
      * @var boolean[]
      */
    protected static array $openAPINullables = [
        'id' => false,
        'group_ringing_time' => false,
        'backup_behaviour' => false,
        'backup_routing_object' => false,
        'backup_routing_object_id' => false
    ];

    /**
      * If a nullable field gets set to null, insert it here
      *
      * @var boolean[]
      */
    protected array $openAPINullablesSetToNull = [];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of nullable properties
     *
     * @return array
     */
    protected static function openAPINullables(): array
    {
        return self::$openAPINullables;
    }

    /**
     * Array of nullable field names deliberately set to null
     *
     * @return boolean[]
     */
    private function getOpenAPINullablesSetToNull(): array
    {
        return $this->openAPINullablesSetToNull;
    }

    /**
     * Setter - Array of nullable field names deliberately set to null
     *
     * @param boolean[] $openAPINullablesSetToNull
     */
    private function setOpenAPINullablesSetToNull(array $openAPINullablesSetToNull): void
    {
        $this->openAPINullablesSetToNull = $openAPINullablesSetToNull;
    }

    /**
     * Checks if a property is nullable
     *
     * @param string $property
     * @return bool
     */
    public static function isNullable(string $property): bool
    {
        return self::openAPINullables()[$property] ?? false;
    }

    /**
     * Checks if a nullable property is set to null.
     *
     * @param string $property
     * @return bool
     */
    public function isNullableSetToNull(string $property): bool
    {
        return in_array($property, $this->getOpenAPINullablesSetToNull(), true);
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
        'group_ringing_time' => 'group_ringing_time',
        'backup_behaviour' => 'backup_behaviour',
        'backup_routing_object' => 'backup_routing_object',
        'backup_routing_object_id' => 'backup_routing_object_id'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
        'group_ringing_time' => 'setGroupRingingTime',
        'backup_behaviour' => 'setBackupBehaviour',
        'backup_routing_object' => 'setBackupRoutingObject',
        'backup_routing_object_id' => 'setBackupRoutingObjectId'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
        'group_ringing_time' => 'getGroupRingingTime',
        'backup_behaviour' => 'getBackupBehaviour',
        'backup_routing_object' => 'getBackupRoutingObject',
        'backup_routing_object_id' => 'getBackupRoutingObjectId'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->setIfExists('id', $data ?? [], null);
        $this->setIfExists('group_ringing_time', $data ?? [], null);
        $this->setIfExists('backup_behaviour', $data ?? [], null);
        $this->setIfExists('backup_routing_object', $data ?? [], null);
        $this->setIfExists('backup_routing_object_id', $data ?? [], null);
    }

    /**
    * Sets $this->container[$variableName] to the given data or to the given default Value; if $variableName
    * is nullable and its value is set to null in the $fields array, then mark it as "set to null" in the
    * $this->openAPINullablesSetToNull array
    *
    * @param string $variableName
    * @param array  $fields
    * @param mixed  $defaultValue
    */
    private function setIfExists(string $variableName, array $fields, $defaultValue): void
    {
        if (self::isNullable($variableName) && array_key_exists($variableName, $fields) && is_null($fields[$variableName])) {
            $this->openAPINullablesSetToNull[] = $variableName;
        }

        $this->container[$variableName] = $fields[$variableName] ?? $defaultValue;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return string|null
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param string|null $id id
     *
     * @return self
     */
    public function setId($id)
    {
        if (is_null($id)) {
            throw new \InvalidArgumentException('non-nullable id cannot be null');
        }
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets group_ringing_time
     *
     * @return string|null
     */
    public function getGroupRingingTime()
    {
        return $this->container['group_ringing_time'];
    }

    /**
     * Sets group_ringing_time
     *
     * @param string|null $group_ringing_time group_ringing_time
     *
     * @return self
     */
    public function setGroupRingingTime($group_ringing_time)
    {
        if (is_null($group_ringing_time)) {
            throw new \InvalidArgumentException('non-nullable group_ringing_time cannot be null');
        }
        $this->container['group_ringing_time'] = $group_ringing_time;

        return $this;
    }

    /**
     * Gets backup_behaviour
     *
     * @return string|null
     */
    public function getBackupBehaviour()
    {
        return $this->container['backup_behaviour'];
    }

    /**
     * Sets backup_behaviour
     *
     * @param string|null $backup_behaviour backup_behaviour
     *
     * @return self
     */
    public function setBackupBehaviour($backup_behaviour)
    {
        if (is_null($backup_behaviour)) {
            throw new \InvalidArgumentException('non-nullable backup_behaviour cannot be null');
        }
        $this->container['backup_behaviour'] = $backup_behaviour;

        return $this;
    }

    /**
     * Gets backup_routing_object
     *
     * @return string|null
     */
    public function getBackupRoutingObject()
    {
        return $this->container['backup_routing_object'];
    }

    /**
     * Sets backup_routing_object
     *
     * @param string|null $backup_routing_object backup_routing_object
     *
     * @return self
     */
    public function setBackupRoutingObject($backup_routing_object)
    {
        if (is_null($backup_routing_object)) {
            throw new \InvalidArgumentException('non-nullable backup_routing_object cannot be null');
        }
        $this->container['backup_routing_object'] = $backup_routing_object;

        return $this;
    }

    /**
     * Gets backup_routing_object_id
     *
     * @return string|null
     */
    public function getBackupRoutingObjectId()
    {
        return $this->container['backup_routing_object_id'];
    }

    /**
     * Sets backup_routing_object_id
     *
     * @param string|null $backup_routing_object_id backup_routing_object_id
     *
     * @return self
     */
    public function setBackupRoutingObjectId($backup_routing_object_id)
    {
        if (is_null($backup_routing_object_id)) {
            throw new \InvalidArgumentException('non-nullable backup_routing_object_id cannot be null');
        }
        $this->container['backup_routing_object_id'] = $backup_routing_object_id;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset): bool
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


