# Placetel PHP OpenAPIClient

---
# Introduction

<h2>Create custom integrations or automate your workflows with the Placetel REST API.</h2>

To make a request to our API, you will need to specify an HTTP **method** and a **path**.
Additionally, you can specify request **headers**, **query** and **body** parameters.
The API will return the response status code, response headers, and dependening on status and resource a response body.

The documentation for every operation displays example requests and responses to provide you the best understanding of our API.

# Pagination

Be aware, that requests which return multiple resources will be paginated by default.
You can specify further pages with the `page` parameter.
For some resources, you can also set a custom page size (usually up to 100) with the `per_page` parameter.
Note that for technical reasons not all endpoints respect the `per_page` parameter.



## Installation & Usage

### Requirements

PHP 7.4 and later.
Should also work with PHP 8.0.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = array('params_example'); // string[]
$mac = 56; // int

try {
    $apiInstance->getCtiMac($params, $mac);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->getCtiMac: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://api.placetel.de/v2*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*CTIApi* | [**getCtiMac**](docs/Api/CTIApi.md#getctimac) | **GET** /cti/{mac} | Get config params
*CTIApi* | [**postCtiMacAnswer**](docs/Api/CTIApi.md#postctimacanswer) | **POST** /cti/{mac}/answer | Answer
*CTIApi* | [**postCtiMacBlindTransfer**](docs/Api/CTIApi.md#postctimacblindtransfer) | **POST** /cti/{mac}/blind_transfer | Blind transfer
*CTIApi* | [**postCtiMacCompleteConference**](docs/Api/CTIApi.md#postctimaccompleteconference) | **POST** /cti/{mac}/complete_conference | Complete conference
*CTIApi* | [**postCtiMacCompleteTransfer**](docs/Api/CTIApi.md#postctimaccompletetransfer) | **POST** /cti/{mac}/complete_transfer | Complete transfer
*CTIApi* | [**postCtiMacDecline**](docs/Api/CTIApi.md#postctimacdecline) | **POST** /cti/{mac}/decline | Decline
*CTIApi* | [**postCtiMacDial**](docs/Api/CTIApi.md#postctimacdial) | **POST** /cti/{mac}/dial | Dial
*CTIApi* | [**postCtiMacDialDigit**](docs/Api/CTIApi.md#postctimacdialdigit) | **POST** /cti/{mac}/dial_digit | Dial digit
*CTIApi* | [**postCtiMacHangup**](docs/Api/CTIApi.md#postctimachangup) | **POST** /cti/{mac}/hangup | Hangup
*CTIApi* | [**postCtiMacHold**](docs/Api/CTIApi.md#postctimachold) | **POST** /cti/{mac}/hold | Hold
*CTIApi* | [**postCtiMacResume**](docs/Api/CTIApi.md#postctimacresume) | **POST** /cti/{mac}/resume | Resume
*CTIApi* | [**postCtiMacSendDtmfDigits**](docs/Api/CTIApi.md#postctimacsenddtmfdigits) | **POST** /cti/{mac}/send_dtmf_digits | Send DTMF Digits
*CTIApi* | [**postCtiMacStartConference**](docs/Api/CTIApi.md#postctimacstartconference) | **POST** /cti/{mac}/start_conference | Start conference
*CTIApi* | [**postCtiMacStartTransfer**](docs/Api/CTIApi.md#postctimacstarttransfer) | **POST** /cti/{mac}/start_transfer | Start transfer
*CTIApi* | [**putCtiMac**](docs/Api/CTIApi.md#putctimac) | **PUT** /cti/{mac} | Set config params
*CallCenterApi* | [**getCallCenterAgents**](docs/Api/CallCenterApi.md#getcallcenteragents) | **GET** /call_center_agents | Fetch all call center agents
*CallCenterApi* | [**getCallCenterCalls**](docs/Api/CallCenterApi.md#getcallcentercalls) | **GET** /call_center_calls | Fetch all call center calls
*CallCenterApi* | [**getCallCenterQueues**](docs/Api/CallCenterApi.md#getcallcenterqueues) | **GET** /call_center_queues | Fetch all call center queues
*CallCenterApi* | [**postCallCenterAgents**](docs/Api/CallCenterApi.md#postcallcenteragents) | **POST** /call_center_agents | Create an agent
*CallCenterApi* | [**postCallCenterQueues**](docs/Api/CallCenterApi.md#postcallcenterqueues) | **POST** /call_center_queues | Create a queue
*CallCenterApi* | [**putCallCenterAgentsId**](docs/Api/CallCenterApi.md#putcallcenteragentsid) | **PUT** /call_center_agents/{id} | Update an agent
*CallCenterApi* | [**putCallCenterQueuesId**](docs/Api/CallCenterApi.md#putcallcenterqueuesid) | **PUT** /call_center_queues/{id} | Update a queue
*CallDetailRecordsApi* | [**getCallDetailRecords**](docs/Api/CallDetailRecordsApi.md#getcalldetailrecords) | **GET** /call_detail_records | Get call detail records
*CallsApi* | [**deleteCallsId**](docs/Api/CallsApi.md#deletecallsid) | **DELETE** /calls/{id} | Delete a call
*CallsApi* | [**getCalls**](docs/Api/CallsApi.md#getcalls) | **GET** /calls | Fetch all incoming calls
*CallsApi* | [**getCallsId**](docs/Api/CallsApi.md#getcallsid) | **GET** /calls/{id} | Retrieve a call
*CallsApi* | [**postCalls**](docs/Api/CallsApi.md#postcalls) | **POST** /calls | Initate a call
*ContactsApi* | [**deleteContactsId**](docs/Api/ContactsApi.md#deletecontactsid) | **DELETE** /contacts/{id} | Deletes a contact
*ContactsApi* | [**getContacts**](docs/Api/ContactsApi.md#getcontacts) | **GET** /contacts | Get all contacts
*ContactsApi* | [**getContactsId**](docs/Api/ContactsApi.md#getcontactsid) | **GET** /contacts/{id} | Retrieve a contact
*ContactsApi* | [**postContacts**](docs/Api/ContactsApi.md#postcontacts) | **POST** /contacts | Create a contact
*ContactsApi* | [**putContactsId**](docs/Api/ContactsApi.md#putcontactsid) | **PUT** /contacts/{id} | Update a contact
*DevicesApi* | [**getDevices**](docs/Api/DevicesApi.md#getdevices) | **GET** /devices | Fetch all devices
*FaxesApi* | [**deleteFaxesId**](docs/Api/FaxesApi.md#deletefaxesid) | **DELETE** /faxes/{id} | Delete a fax
*FaxesApi* | [**getFaxes**](docs/Api/FaxesApi.md#getfaxes) | **GET** /faxes | Fetch in- and outbound faxes
*FaxesApi* | [**getFaxesId**](docs/Api/FaxesApi.md#getfaxesid) | **GET** /faxes/{id} | Fetch specific fax
*FaxesApi* | [**postFaxes**](docs/Api/FaxesApi.md#postfaxes) | **POST** /faxes | Create outbound fax
*GroupsApi* | [**deleteGroupsId**](docs/Api/GroupsApi.md#deletegroupsid) | **DELETE** /groups/{id} | Delete a group
*GroupsApi* | [**getGroups**](docs/Api/GroupsApi.md#getgroups) | **GET** /groups | Fetch all groups
*GroupsApi* | [**getGroupsId**](docs/Api/GroupsApi.md#getgroupsid) | **GET** /groups/{id} | Fetch a group
*GroupsApi* | [**postGroups**](docs/Api/GroupsApi.md#postgroups) | **POST** /groups | Create a group
*GroupsApi* | [**putGroupsId**](docs/Api/GroupsApi.md#putgroupsid) | **PUT** /groups/{id} | Update a group
*NumbersApi* | [**deleteNumbersNumberIdProfilesId**](docs/Api/NumbersApi.md#deletenumbersnumberidprofilesid) | **DELETE** /numbers/{number_id}/profiles/{id} | Delete a profile for a number
*NumbersApi* | [**getNumbers**](docs/Api/NumbersApi.md#getnumbers) | **GET** /numbers | Get all numbers
*NumbersApi* | [**getNumbersId**](docs/Api/NumbersApi.md#getnumbersid) | **GET** /numbers/{id} | Retrieve a number
*NumbersApi* | [**getNumbersNumberIdProfiles**](docs/Api/NumbersApi.md#getnumbersnumberidprofiles) | **GET** /numbers/{number_id}/profiles | List profiles for a number
*NumbersApi* | [**postNumbersNumberIdProfiles**](docs/Api/NumbersApi.md#postnumbersnumberidprofiles) | **POST** /numbers/{number_id}/profiles | Creates a new profile for a number
*NumbersApi* | [**putNumbersNumberIdProfiles**](docs/Api/NumbersApi.md#putnumbersnumberidprofiles) | **PUT** /numbers/{number_id}/profiles | Activates a profile for a number
*PromptsApi* | [**deletePromptsId**](docs/Api/PromptsApi.md#deletepromptsid) | **DELETE** /prompts/{id} | Delete a prompt
*PromptsApi* | [**getPrompts**](docs/Api/PromptsApi.md#getprompts) | **GET** /prompts | Fetch all prompts
*PromptsApi* | [**getPromptsId**](docs/Api/PromptsApi.md#getpromptsid) | **GET** /prompts/{id} | Retrieve a prompt
*PromptsApi* | [**postPrompts**](docs/Api/PromptsApi.md#postprompts) | **POST** /prompts | Create a prompt
*PromptsApi* | [**putPromptsId**](docs/Api/PromptsApi.md#putpromptsid) | **PUT** /prompts/{id} | Update a prompt
*ProvisioningsApi* | [**deleteProvisioningsId**](docs/Api/ProvisioningsApi.md#deleteprovisioningsid) | **DELETE** /provisionings/{id} | Delete a phone provisionings
*ProvisioningsApi* | [**deleteProvisioningsIdCustomConfigurationsCustomConfigurationId**](docs/Api/ProvisioningsApi.md#deleteprovisioningsidcustomconfigurationscustomconfigurationid) | **DELETE** /provisionings/{id}/custom_configurations/{custom_configuration_id} | Delete a phone provisionings custom configuration key
*ProvisioningsApi* | [**getProvisionings**](docs/Api/ProvisioningsApi.md#getprovisionings) | **GET** /provisionings | Get all phone provisionings
*ProvisioningsApi* | [**getProvisioningsId**](docs/Api/ProvisioningsApi.md#getprovisioningsid) | **GET** /provisionings/{id} | Retrieve a phone provisionings
*ProvisioningsApi* | [**getProvisioningsIdCustomConfigurations**](docs/Api/ProvisioningsApi.md#getprovisioningsidcustomconfigurations) | **GET** /provisionings/{id}/custom_configurations | Retrieve a phone provisionings custom configurations
*ProvisioningsApi* | [**postProvisionings**](docs/Api/ProvisioningsApi.md#postprovisionings) | **POST** /provisionings | Creates a phone provisionings
*ProvisioningsApi* | [**postProvisioningsIdReconfigure**](docs/Api/ProvisioningsApi.md#postprovisioningsidreconfigure) | **POST** /provisionings/{id}/reconfigure | Trigger reconfiguration of a phone
*ProvisioningsApi* | [**putProvisioningsId**](docs/Api/ProvisioningsApi.md#putprovisioningsid) | **PUT** /provisionings/{id} | Update a phone provisionings
*ProvisioningsApi* | [**putProvisioningsIdCustomConfigurations**](docs/Api/ProvisioningsApi.md#putprovisioningsidcustomconfigurations) | **PUT** /provisionings/{id}/custom_configurations | Update or create a phone provisionings custom configuration key
*RecordingsApi* | [**deleteRecordingsId**](docs/Api/RecordingsApi.md#deleterecordingsid) | **DELETE** /recordings/{id} | Delete a recording
*RecordingsApi* | [**getRecordings**](docs/Api/RecordingsApi.md#getrecordings) | **GET** /recordings | Fetch call recordings
*RecordingsApi* | [**getRecordingsId**](docs/Api/RecordingsApi.md#getrecordingsid) | **GET** /recordings/{id} | Fetch specific recording
*RoutingPlanObjectsApi* | [**getRoutingPlansIdRoutings**](docs/Api/RoutingPlanObjectsApi.md#getroutingplansidroutings) | **GET** /routing_plans/{id}/routings | Get a routing plans objects
*RoutingPlansApi* | [**deleteRoutingPlansId**](docs/Api/RoutingPlansApi.md#deleteroutingplansid) | **DELETE** /routing_plans/{id} | Delete a routing plan
*RoutingPlansApi* | [**getRoutingPlans**](docs/Api/RoutingPlansApi.md#getroutingplans) | **GET** /routing_plans | Get all routing plans
*RoutingPlansApi* | [**getRoutingPlansId**](docs/Api/RoutingPlansApi.md#getroutingplansid) | **GET** /routing_plans/{id} | Get a routing plan
*RoutingPlansApi* | [**getRoutingPlansIdSimulate**](docs/Api/RoutingPlansApi.md#getroutingplansidsimulate) | **GET** /routing_plans/{id}/simulate | Simulate the usage of a routing plan
*RoutingPlansApi* | [**postRoutingPlans**](docs/Api/RoutingPlansApi.md#postroutingplans) | **POST** /routing_plans | Create a routing plan
*RoutingPlansApi* | [**putRoutingPlansId**](docs/Api/RoutingPlansApi.md#putroutingplansid) | **PUT** /routing_plans/{id} | Update a routing plan
*RoutingsApi* | [**deleteRoutingsNumberOrId**](docs/Api/RoutingsApi.md#deleteroutingsnumberorid) | **DELETE** /routings/{number_or_id} | Delete a routing object
*RoutingsApi* | [**getRoutings**](docs/Api/RoutingsApi.md#getroutings) | **GET** /routings | Get all routings
*RoutingsApi* | [**getRoutingsNumberOrId**](docs/Api/RoutingsApi.md#getroutingsnumberorid) | **GET** /routings/{number_or_id} | Retrieve routing
*RoutingsApi* | [**postRoutings**](docs/Api/RoutingsApi.md#postroutings) | **POST** /routings | Create a routing object
*RoutingsApi* | [**putRoutingsNumberOrId**](docs/Api/RoutingsApi.md#putroutingsnumberorid) | **PUT** /routings/{number_or_id} | Update routing
*SMSApi* | [**postSms**](docs/Api/SMSApi.md#postsms) | **POST** /sms | Send SMS
*SipUsersApi* | [**deleteSipUsersId**](docs/Api/SipUsersApi.md#deletesipusersid) | **DELETE** /sip_users/{id} | Delete a sip user
*SipUsersApi* | [**deleteSipUsersIdShortCodesCodeId**](docs/Api/SipUsersApi.md#deletesipusersidshortcodescodeid) | **DELETE** /sip_users/{id}/short_codes/{code_id} | Delete short code
*SipUsersApi* | [**getSipUsers**](docs/Api/SipUsersApi.md#getsipusers) | **GET** /sip_users | Fetch all sip users
*SipUsersApi* | [**getSipUsersId**](docs/Api/SipUsersApi.md#getsipusersid) | **GET** /sip_users/{id} | Fetch a sip user
*SipUsersApi* | [**getSipUsersIdShortCodes**](docs/Api/SipUsersApi.md#getsipusersidshortcodes) | **GET** /sip_users/{id}/short_codes | Short codes
*SipUsersApi* | [**postSipUsers**](docs/Api/SipUsersApi.md#postsipusers) | **POST** /sip_users | Create a sip user
*SipUsersApi* | [**postSipUsersIdShortCodes**](docs/Api/SipUsersApi.md#postsipusersidshortcodes) | **POST** /sip_users/{id}/short_codes | Create short code
*SipUsersApi* | [**putSipUsersId**](docs/Api/SipUsersApi.md#putsipusersid) | **PUT** /sip_users/{id} | Update a sip user
*SubscriptionsApi* | [**deleteSubscriptionsId**](docs/Api/SubscriptionsApi.md#deletesubscriptionsid) | **DELETE** /subscriptions/{id} | Delete a subscription
*SubscriptionsApi* | [**getSubscriptions**](docs/Api/SubscriptionsApi.md#getsubscriptions) | **GET** /subscriptions | List subscriptions
*SubscriptionsApi* | [**putSubscriptions**](docs/Api/SubscriptionsApi.md#putsubscriptions) | **PUT** /subscriptions | Add a subscription
*UsersApi* | [**deleteUsersId**](docs/Api/UsersApi.md#deleteusersid) | **DELETE** /users/{id} | Delete a subuser
*UsersApi* | [**getMe**](docs/Api/UsersApi.md#getme) | **GET** /me | Me
*UsersApi* | [**getUsers**](docs/Api/UsersApi.md#getusers) | **GET** /users | Fetch all users
*UsersApi* | [**getUsersId**](docs/Api/UsersApi.md#getusersid) | **GET** /users/{id} | Fetch user by ID
*UsersApi* | [**postUsers**](docs/Api/UsersApi.md#postusers) | **POST** /users | Create new subuser
*UsersApi* | [**postUsersIdPasswordReset**](docs/Api/UsersApi.md#postusersidpasswordreset) | **POST** /users/{id}/password_reset | Reset password for account
*UsersApi* | [**putUsersId**](docs/Api/UsersApi.md#putusersid) | **PUT** /users/{id} | Update a subuser

## Models

- [Call](docs/Model/Call.md)
- [CallCenterAgent](docs/Model/CallCenterAgent.md)
- [CallCenterCall](docs/Model/CallCenterCall.md)
- [CallCenterQueue](docs/Model/CallCenterQueue.md)
- [CallDetailRecord](docs/Model/CallDetailRecord.md)
- [CallToNumber](docs/Model/CallToNumber.md)
- [Contact](docs/Model/Contact.md)
- [CustomConfiguration](docs/Model/CustomConfiguration.md)
- [Destination](docs/Model/Destination.md)
- [Device](docs/Model/Device.md)
- [Fax](docs/Model/Fax.md)
- [Group](docs/Model/Group.md)
- [Key](docs/Model/Key.md)
- [Line](docs/Model/Line.md)
- [Me](docs/Model/Me.md)
- [Number](docs/Model/Number.md)
- [Permission](docs/Model/Permission.md)
- [PostCallCenterAgents](docs/Model/PostCallCenterAgents.md)
- [PostCallCenterQueues](docs/Model/PostCallCenterQueues.md)
- [PostCalls](docs/Model/PostCalls.md)
- [PostContacts](docs/Model/PostContacts.md)
- [PostCtiMacAnswer](docs/Model/PostCtiMacAnswer.md)
- [PostCtiMacBlindTransfer](docs/Model/PostCtiMacBlindTransfer.md)
- [PostCtiMacCompleteConference](docs/Model/PostCtiMacCompleteConference.md)
- [PostCtiMacCompleteTransfer](docs/Model/PostCtiMacCompleteTransfer.md)
- [PostCtiMacDecline](docs/Model/PostCtiMacDecline.md)
- [PostCtiMacDial](docs/Model/PostCtiMacDial.md)
- [PostCtiMacDialDigit](docs/Model/PostCtiMacDialDigit.md)
- [PostCtiMacHangup](docs/Model/PostCtiMacHangup.md)
- [PostCtiMacHold](docs/Model/PostCtiMacHold.md)
- [PostCtiMacResume](docs/Model/PostCtiMacResume.md)
- [PostCtiMacSendDtmfDigits](docs/Model/PostCtiMacSendDtmfDigits.md)
- [PostCtiMacStartConference](docs/Model/PostCtiMacStartConference.md)
- [PostCtiMacStartTransfer](docs/Model/PostCtiMacStartTransfer.md)
- [PostFaxes](docs/Model/PostFaxes.md)
- [PostGroups](docs/Model/PostGroups.md)
- [PostNumbersNumberIdProfiles](docs/Model/PostNumbersNumberIdProfiles.md)
- [PostPrompts](docs/Model/PostPrompts.md)
- [PostProvisionings](docs/Model/PostProvisionings.md)
- [PostRoutingPlans](docs/Model/PostRoutingPlans.md)
- [PostRoutings](docs/Model/PostRoutings.md)
- [PostRoutingsTimeSettings](docs/Model/PostRoutingsTimeSettings.md)
- [PostSipUsers](docs/Model/PostSipUsers.md)
- [PostSipUsersIdShortCodes](docs/Model/PostSipUsersIdShortCodes.md)
- [PostSms](docs/Model/PostSms.md)
- [PostUsers](docs/Model/PostUsers.md)
- [Profile](docs/Model/Profile.md)
- [Prompt](docs/Model/Prompt.md)
- [Provisioning](docs/Model/Provisioning.md)
- [PutCallCenterAgents](docs/Model/PutCallCenterAgents.md)
- [PutCallCenterQueues](docs/Model/PutCallCenterQueues.md)
- [PutContacts](docs/Model/PutContacts.md)
- [PutCti](docs/Model/PutCti.md)
- [PutGroups](docs/Model/PutGroups.md)
- [PutNumbersNumberIdProfiles](docs/Model/PutNumbersNumberIdProfiles.md)
- [PutPrompts](docs/Model/PutPrompts.md)
- [PutProvisionings](docs/Model/PutProvisionings.md)
- [PutProvisioningsIdCustomConfigurations](docs/Model/PutProvisioningsIdCustomConfigurations.md)
- [PutProvisioningsIdCustomConfigurationsConfigurations](docs/Model/PutProvisioningsIdCustomConfigurationsConfigurations.md)
- [PutProvisioningsKeysInner](docs/Model/PutProvisioningsKeysInner.md)
- [PutRoutingPlans](docs/Model/PutRoutingPlans.md)
- [PutRoutings](docs/Model/PutRoutings.md)
- [PutRoutingsApi](docs/Model/PutRoutingsApi.md)
- [PutRoutingsCustomCallerid](docs/Model/PutRoutingsCustomCallerid.md)
- [PutRoutingsForward](docs/Model/PutRoutingsForward.md)
- [PutRoutingsForwardDestinationsInner](docs/Model/PutRoutingsForwardDestinationsInner.md)
- [PutRoutingsGroup](docs/Model/PutRoutingsGroup.md)
- [PutRoutingsIvr](docs/Model/PutRoutingsIvr.md)
- [PutRoutingsMailbox](docs/Model/PutRoutingsMailbox.md)
- [PutRoutingsNotification](docs/Model/PutRoutingsNotification.md)
- [PutRoutingsPlan](docs/Model/PutRoutingsPlan.md)
- [PutRoutingsQueue](docs/Model/PutRoutingsQueue.md)
- [PutRoutingsTimeSettings](docs/Model/PutRoutingsTimeSettings.md)
- [PutSipUsers](docs/Model/PutSipUsers.md)
- [PutSubscriptions](docs/Model/PutSubscriptions.md)
- [PutUsers](docs/Model/PutUsers.md)
- [PutUsersPermissions](docs/Model/PutUsersPermissions.md)
- [Recording](docs/Model/Recording.md)
- [RoutingObject](docs/Model/RoutingObject.md)
- [RoutingObjectApi](docs/Model/RoutingObjectApi.md)
- [RoutingObjectCustomCallerId](docs/Model/RoutingObjectCustomCallerId.md)
- [RoutingObjectForward](docs/Model/RoutingObjectForward.md)
- [RoutingObjectGroup](docs/Model/RoutingObjectGroup.md)
- [RoutingObjectIvr](docs/Model/RoutingObjectIvr.md)
- [RoutingObjectMailbox](docs/Model/RoutingObjectMailbox.md)
- [RoutingObjectNotification](docs/Model/RoutingObjectNotification.md)
- [RoutingObjectPlan](docs/Model/RoutingObjectPlan.md)
- [RoutingObjectTimeSettings](docs/Model/RoutingObjectTimeSettings.md)
- [RoutingPlan](docs/Model/RoutingPlan.md)
- [ShortCode](docs/Model/ShortCode.md)
- [SipUser](docs/Model/SipUser.md)
- [Subscription](docs/Model/Subscription.md)
- [User](docs/Model/User.md)

## Authorization

Authentication schemes defined for the API:
### Bearer

- **Type**: API key
- **API key parameter name**: Authorization
- **Location**: HTTP header


## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `2.0.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`

### Regenerate API lib
```
wget https://api.placetel.de/v2/swagger_doc
docker run --rm \
    -v $PWD:/local openapitools/openapi-generator-cli generate \
    -i /local/swagger_doc.json \
    -g php \
    -o /local/out/php
```
