# # PostUsers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **string** |  |
**last_name** | **string** |  |
**email** | **string** | A subuser without email won&#39;t be able to log in to the web portal | [optional]
**did** | **int** |  | [optional]
**assign_did** | **bool** | Auto assign DID | [optional] [default to false]
**callerid** | **string** | Phone number which will be visible in outgoing calls | [optional] [default to '']
**phone_model** | **string** | Name of your phone. Have look at the Devices#index API endpoint | [optional]
**phone_mac** | **string** |  | [optional]
**sip_user_type** | **string** |  | [optional] [default to 'standard']

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
