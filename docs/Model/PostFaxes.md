# # PostFaxes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from_number** | **string** |  |
**to_number** | **string** |  |
**email** | **string** |  |
**file** | **string** | Base64 encoded PDF file as [data uri](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs) |
**header** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
