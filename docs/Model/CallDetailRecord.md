# # CallDetailRecord

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **float** |  | [optional]
**description** | **string** |  | [optional]
**from** | **string** |  | [optional]
**to** | **string** |  | [optional]
**length** | **int** |  | [optional]
**answer_uri** | **string** |  | [optional]
**received_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
