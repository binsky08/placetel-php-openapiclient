# # PutUsers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **string** |  | [optional]
**last_name** | **string** |  | [optional]
**email** | **string** |  | [optional]
**did** | **int** | Internal DID | [optional]
**callerid** | **string** | Outgoing caller ID | [optional]
**primary_sip_user_id** | **int** |  | [optional]
**admin_user** | **bool** | Grant admin rights | [optional]
**rights_numbers** | **string[]** | Grant rights for these numbers | [optional]
**locale** | **string** | Language (ISO-639-1 Code) only de / en is supported | [optional]
**obfuscate_billing_records** | **bool** | Obfuscate billing records for this user | [optional]
**obfuscate_others** | **bool** | Obfuscate numbers not belonging to this user | [optional]
**permissions** | [**\OpenAPI\Client\Model\PutUsersPermissions**](PutUsersPermissions.md) |  | [optional]
**avatar** | **string** | Base64 encoded image as [data uri](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
