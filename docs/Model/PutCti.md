# # PutCti

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**params** | **object** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
