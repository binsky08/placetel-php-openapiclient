# # SipUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Internal ID of the SIP user | [optional]
**sipuid** | **string** | SIPUID used for SIP communication | [optional]
**username** | **string** | Username used for SIP communication | [optional]
**password** | **string** | Password used for SIP communication (only visible when fetching a single record or after creating one; not for Placetel MOBILE users) | [optional]
**domain** | **string** | Domain used for SIP communication | [optional]
**name** | **string** |  | [optional]
**description** | **string** |  | [optional]
**did** | **int** | Internal extension of this SIP user | [optional]
**callerid** | **string** | The caller ID for outbound calls | [optional]
**type** | **string** |  | [optional]
**webuser_id** | **int** | Placetel user connected with this SIP user | [optional]
**webuser** | [**\OpenAPI\Client\Model\User**](User.md) |  | [optional]
**automatic_prefix** | **string** | Prepend this prefix for outbound calls automatically | [optional]
**blocked_prefixes** | **string** | A comma separated list of blocked destinations | [optional]
**routing_plan_id** | **int** | Fallback routing plan for busy/no answer | [optional]
**online** | **bool** | Current online status | [optional]
**registrations** | **string** |  | [optional]
**contact_speeddialing** | **bool** |  | [optional]
**p_asserted_identity** | **string** |  | [optional]
**hotdesk_login** | **string** |  | [optional]
**hotdesk_pin** | **string** |  | [optional]
**hotdesk_type** | **string** |  | [optional]
**hotdesk_provisioning_id** | **string** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
