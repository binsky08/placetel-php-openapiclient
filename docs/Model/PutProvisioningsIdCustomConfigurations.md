# # PutProvisioningsIdCustomConfigurations

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configurations** | [**\OpenAPI\Client\Model\PutProvisioningsIdCustomConfigurationsConfigurations**](PutProvisioningsIdCustomConfigurationsConfigurations.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
