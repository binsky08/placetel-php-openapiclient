# # PostCallCenterQueues

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the agent |
**num_loops** | **int** |  | [optional] [default to 3]
**priority** | **int** |  | [optional] [default to 0]
**description** | **string** |  | [optional]
**mail_notification** | **string** |  | [optional] [default to '']
**ringing_time** | **int** |  | [optional] [default to 30]
**max_iterations** | **int** |  | [optional] [default to 10]
**play_prompt** | **bool** |  | [optional] [default to false]
**prompt_id** | **int** | ID of a prompt | [optional]
**play_prompt_waiting** | **bool** |  | [optional] [default to false]
**prompt_waiting_id** | **int** | ID of a prompt | [optional]
**play_prompt_end** | **bool** |  | [optional] [default to false]
**prompt_end_id** | **int** | ID of a prompt | [optional]
**play_mailbox** | **bool** |  | [optional] [default to false]
**prompt_mailbox_id** | **int** | ID of a prompt | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
