# # PostSms

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recipient** | **string** | Number to send message to |
**message** | **string** | Your message |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
