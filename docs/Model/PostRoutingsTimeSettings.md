# # PostRoutingsTimeSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** |  | [optional]
**mon** | **bool** | Activates routing objects for mondays | [optional]
**tue** | **bool** | Activates routing objects for tuesdays | [optional]
**wed** | **bool** | Activates routing objects for wednesdays | [optional]
**thu** | **bool** | Activates routing objects for thursdays | [optional]
**fri** | **bool** | Activates routing objects for fridays | [optional]
**sat** | **bool** | Activates routing objects for saturdays | [optional]
**sun** | **bool** | Activates routing objects for sundays | [optional]
**extended_time_settings** | **bool** | Activates extended time settings options | [optional]
**time_from** | **string** | Only used when &#x60;extended_time_settings&#x60; is &#x60;false&#x60;.&lt;br&gt; Example: &#x60;08:00:00&#x60; | [optional]
**time_to** | **string** | Only used when &#x60;extended_time_settings&#x60; is &#x60;false&#x60;.&lt;br&gt; Example: &#x60;19:59:59&#x60; | [optional]
**additional_dates** | **string** | Additional dates w/o years to activate routing object for.&lt;br&gt; Only used when &#x60;extended_time_settings&#x60; is &#x60;true&#x60;.&lt;br&gt;Example: &#x60;9.9,2.1,24.12,25.12,31.12&#x60; | [optional]
**time_ranges** | **string** | Time ranges to activate routing object for.&lt;br&gt; Only used when &#x60;extended_time_settings&#x60; is &#x60;true&#x60;.&lt;br&gt;Example: &#x60;08:30:00-13:29:59,14:30:00-17:59:59&#x60; | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
