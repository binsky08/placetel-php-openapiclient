# # Me

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**name** | **string** |  | [optional]
**email** | **string** |  | [optional]
**company** | **string** |  | [optional]
**primary_sip_user_id** | **int** |  | [optional]
**platform** | **string** |  | [optional]
**rights_numbers** | **string[]** |  | [optional]
**main_account_id** | **int** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
