# # PostCtiMacHangup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**line** | **int** |  |
**call_id** | **int** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
