# # PostCallCenterAgents

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the agent |
**extension_type** | **string** | Extension type |
**extension_sip** | **string** | SIP extension of the agent |
**extension_pstn** | **string** | PSTN extension of the agent |
**agent_type** | **string** | Type of the agent | [optional] [default to 'Agent']
**description** | **string** | Describes the agent | [optional]
**priority** | **int** | Priority of the agent | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
