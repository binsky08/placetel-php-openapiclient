# # PostSipUsers

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  |
**type** | **string** |  |
**description** | **string** |  | [optional]
**did** | **int** |  | [optional]
**callerid** | **string** |  | [optional]
**webuser_id** | **int** |  | [optional]
**contact_speeddialing** | **bool** |  | [optional]
**automatic_prefix** | **string** |  | [optional]
**blocked_prefixes** | **string** |  | [optional]
**routing_plan_id** | **int** |  | [optional]
**hotdesk_login** | **string** |  | [optional]
**hotdesk_pin** | **string** |  | [optional]
**hotdesk_type** | **string** |  | [optional]
**hotdesk_provisioning_id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
