# # RoutingObjectTimeSettings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** |  | [optional]
**mon** | **bool** |  | [optional]
**tue** | **bool** |  | [optional]
**wed** | **bool** |  | [optional]
**thu** | **bool** |  | [optional]
**fri** | **bool** |  | [optional]
**sat** | **bool** |  | [optional]
**sun** | **bool** |  | [optional]
**extended_time_settings** | **bool** |  | [optional]
**time_from** | **string** |  | [optional]
**time_to** | **string** |  | [optional]
**additional_dates** | **string** |  | [optional]
**time_ranges** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
