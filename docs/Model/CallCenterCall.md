# # CallCenterCall

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**call_type** | **string** |  | [optional]
**call_status** | **string** |  | [optional]
**answer_status** | **string** |  | [optional]
**started_at** | **\DateTime** |  | [optional]
**left_queue_at** | **\DateTime** |  | [optional]
**ended_at** | **\DateTime** |  | [optional]
**last_updated_at** | **\DateTime** |  | [optional]
**iteration** | **string** |  | [optional]
**from** | **string** |  | [optional]
**to** | **string** |  | [optional]
**queue_name** | **string** |  | [optional]
**agent_name** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
