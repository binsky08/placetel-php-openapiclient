# # PutRoutingsForwardDestinationsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targets** | **string[]** | sipuid of a sipuser or external phone number |
**ringing_time** | **int** |  | [optional] [default to self::RINGING_TIME_60]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
