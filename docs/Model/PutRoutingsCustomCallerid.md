# # PutRoutingsCustomCallerid

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**callerid_enabled** | **bool** |  | [optional]
**callerid_name** | **string** |  | [optional]
**callerid_number** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
