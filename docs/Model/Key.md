# # Key

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional]
**label** | **string** |  | [optional]
**value** | **string** |  | [optional]
**position** | **string** |  | [optional]
**extended** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
