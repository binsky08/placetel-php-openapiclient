# # Call

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**type** | **string** |  | [optional]
**from_number** | **string** |  | [optional]
**to_number** | [**\OpenAPI\Client\Model\CallToNumber**](CallToNumber.md) |  | [optional]
**file_url** | **string** |  | [optional]
**duration** | **int** |  | [optional]
**contact** | [**\OpenAPI\Client\Model\Contact**](Contact.md) |  | [optional]
**received_at** | **\DateTime** |  | [optional]
**unread** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
