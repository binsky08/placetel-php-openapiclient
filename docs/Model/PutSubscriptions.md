# # PutSubscriptions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**service** | **string** |  |
**url** | **string** |  |
**incoming** | **bool** | Incoming calls | [optional] [default to false]
**outgoing** | **bool** | Outgoing calls | [optional] [default to false]
**hungup** | **bool** | Call hungup | [optional] [default to false]
**accepted** | **bool** | Call accepted | [optional] [default to false]
**phone** | **bool** | CTI events | [optional] [default to false]
**numbers** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
