# # PostPrompts

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the resource |
**description** | **string** | More detailed description of the resource | [optional]
**text** | **string** | Text used to synthesize the prompt | [optional]
**voice** | **string** | Voice used to synthesize the prompt |
**file** | **string** | Base64 encoded audio file as [data uri](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs) | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
