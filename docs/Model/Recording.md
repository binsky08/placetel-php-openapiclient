# # Recording

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**direction** | **string** |  | [optional]
**from** | **string** |  | [optional]
**to** | **string** |  | [optional]
**duration** | **int** |  | [optional]
**time** | **\DateTime** |  | [optional]
**file** | **string** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
