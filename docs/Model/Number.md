# # Number

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**routing_id** | **string** |  | [optional]
**prefix** | **string** |  | [optional]
**numonly** | **string** |  | [optional]
**number** | **string** |  | [optional]
**activated** | **bool** |  | [optional]
**test_number** | **bool** |  | [optional]
**name** | **string** |  | [optional]
**description** | **string** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
