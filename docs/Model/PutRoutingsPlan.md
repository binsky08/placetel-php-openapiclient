# # PutRoutingsPlan

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  |
**destinations** | [**\OpenAPI\Client\Model\PutRoutingsForwardDestinationsInner[]**](PutRoutingsForwardDestinationsInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
