# # RoutingObjectForward

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**behaviour** | **string** |  | [optional]
**prompt_id** | **int** |  | [optional]
**destinations** | [**\OpenAPI\Client\Model\Destination[]**](Destination.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
