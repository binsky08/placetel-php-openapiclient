# # RoutingObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**name** | **string** |  | [optional]
**forward** | [**\OpenAPI\Client\Model\RoutingObjectForward**](RoutingObjectForward.md) |  | [optional]
**group** | [**\OpenAPI\Client\Model\RoutingObjectGroup**](RoutingObjectGroup.md) |  | [optional]
**plan** | [**\OpenAPI\Client\Model\RoutingObjectPlan**](RoutingObjectPlan.md) |  | [optional]
**ivr** | [**\OpenAPI\Client\Model\RoutingObjectIvr**](RoutingObjectIvr.md) |  | [optional]
**queue** | [**\OpenAPI\Client\Model\CallCenterQueue**](CallCenterQueue.md) |  | [optional]
**api** | [**\OpenAPI\Client\Model\RoutingObjectApi**](RoutingObjectApi.md) |  | [optional]
**mailbox** | [**\OpenAPI\Client\Model\RoutingObjectMailbox**](RoutingObjectMailbox.md) |  | [optional]
**notification** | [**\OpenAPI\Client\Model\RoutingObjectNotification**](RoutingObjectNotification.md) |  | [optional]
**time_settings** | [**\OpenAPI\Client\Model\RoutingObjectTimeSettings**](RoutingObjectTimeSettings.md) |  | [optional]
**custom_caller_id** | [**\OpenAPI\Client\Model\RoutingObjectCustomCallerId**](RoutingObjectCustomCallerId.md) |  | [optional]
**notify_external_api** | **string** |  | [optional]
**music_on_hold** | **string** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
