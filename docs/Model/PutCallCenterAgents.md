# # PutCallCenterAgents

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional]
**priority** | **int** | Priority of the agent | [optional]
**description** | **string** | Describes the agent | [optional]
**status** | **string** |  | [optional]
**extension_type** | **string** | Extension type | [optional]
**extension_sip** | **string** | SIP extension of the agent |
**extension_pstn** | **string** | PSTN extension of the agent |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
