# # Contact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**user_id** | **int** |  | [optional]
**speeddial** | **int** |  | [optional]
**first_name** | **string** |  | [optional]
**last_name** | **string** |  | [optional]
**email** | **string** |  | [optional]
**email_work** | **string** |  | [optional]
**company** | **string** |  | [optional]
**address** | **string** |  | [optional]
**address_work** | **string** |  | [optional]
**phone_work** | **string** |  | [optional]
**mobile_work** | **string** |  | [optional]
**phone** | **string** |  | [optional]
**mobile** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**fax_work** | **string** |  | [optional]
**facebook_url** | **string** |  | [optional]
**linkedin_url** | **string** |  | [optional]
**xing_url** | **string** |  | [optional]
**twitter_account** | **string** |  | [optional]
**blocked** | **bool** |  | [optional]
**color** | **string** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
