# # Line

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sipuid** | **string** |  | [optional]
**position** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
