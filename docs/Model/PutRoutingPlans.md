# # PutRoutingPlans

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  |
**description** | **string** |  | [optional]
**did** | **string** |  | [optional]
**routing_objects** | [**\OpenAPI\Client\Model\PutRoutingsQueue[]**](PutRoutingsQueue.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
