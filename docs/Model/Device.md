# # Device

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**name** | **string** |  | [optional]
**display_name** | **string** |  | [optional]
**manufacturer** | **string** |  | [optional]
**lines** | **string** |  | [optional]
**blf_keys** | **string** |  | [optional]
**expansion_keys** | **string** |  | [optional]
**max_expansion_modules** | **string** |  | [optional]
**dect_lines** | **string** |  | [optional]
**multicell** | **string** |  | [optional]
**encryptable** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
