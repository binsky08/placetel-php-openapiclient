# # RoutingObjectApi

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_retries** | **string** |  | [optional]
**backup_routing_plan_id** | **string** |  | [optional]
**prompt** | [**\OpenAPI\Client\Model\Prompt**](Prompt.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
