# # User

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**login** | **string** |  | [optional]
**email** | **string** |  | [optional]
**first_name** | **string** |  | [optional]
**last_name** | **string** |  | [optional]
**type** | **string** |  | [optional]
**did** | **int** | Internal DID of primary SIP user | [optional]
**callerid** | **string** | Outgoing caller ID of primary SIP user | [optional]
**primary_sip_user_id** | **int** |  | [optional]
**locale** | **string** | Language (ISO-639-1 Code) | [optional]
**obfuscate_billing_records** | **bool** | Obfuscate billing records for this user | [optional]
**obfuscate_others** | **bool** | Obfuscate numbers not in rights_numbers for this user | [optional]
**admin_user** | **bool** |  | [optional]
**rights_numbers** | **string[]** |  | [optional]
**permissions** | [**\OpenAPI\Client\Model\Permission**](Permission.md) |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
