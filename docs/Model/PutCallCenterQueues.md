# # PutCallCenterQueues

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Name of the agent | [optional]
**num_loops** | **int** |  | [optional]
**priority** | **int** |  | [optional]
**description** | **string** |  | [optional]
**mail_notification** | **string** |  | [optional]
**ringing_time** | **int** |  | [optional]
**max_iterations** | **int** |  | [optional]
**play_prompt** | **bool** |  | [optional]
**prompt_id** | **int** | ID of a prompt | [optional]
**play_prompt_waiting** | **bool** |  | [optional]
**prompt_waiting_id** | **int** | ID of a prompt | [optional]
**play_prompt_end** | **bool** |  | [optional]
**prompt_end_id** | **int** | ID of a prompt | [optional]
**play_mailbox** | **bool** |  | [optional]
**prompt_mailbox_id** | **int** | ID of a prompt | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
