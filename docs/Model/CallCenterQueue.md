# # CallCenterQueue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**name** | **string** |  | [optional]
**num_loops** | **string** |  | [optional]
**priority** | **string** |  | [optional]
**description** | **string** |  | [optional]
**mail_notification** | **string** |  | [optional]
**ringing_time** | **string** |  | [optional]
**max_iterations** | **string** |  | [optional]
**play_prompt** | **string** |  | [optional]
**prompt** | [**\OpenAPI\Client\Model\Prompt**](Prompt.md) |  | [optional]
**play_prompt_waiting** | **string** |  | [optional]
**prompt_waiting** | [**\OpenAPI\Client\Model\Prompt**](Prompt.md) |  | [optional]
**play_prompt_end** | **string** |  | [optional]
**prompt_end** | [**\OpenAPI\Client\Model\Prompt**](Prompt.md) |  | [optional]
**play_mailbox** | **string** |  | [optional]
**prompt_mailbox** | [**\OpenAPI\Client\Model\Prompt**](Prompt.md) |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
