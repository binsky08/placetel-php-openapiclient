# # Destination

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**targets** | **string[]** | sipuid of a sipuser or external phone number | [optional]
**ringing_time** | **string** |  | [optional] [default to '60']

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
