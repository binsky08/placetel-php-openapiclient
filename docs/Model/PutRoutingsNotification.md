# # PutRoutingsNotification

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**salutation** | **string** |  | [optional]
**email** | **string** |  | [optional]
**type** | **string** |  | [optional]
**receiver** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
