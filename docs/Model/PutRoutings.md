# # PutRoutings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | A name for the routing object | [optional]
**forward** | [**\OpenAPI\Client\Model\PutRoutingsForward**](PutRoutingsForward.md) |  | [optional]
**group** | [**\OpenAPI\Client\Model\PutRoutingsGroup**](PutRoutingsGroup.md) |  | [optional]
**plan** | [**\OpenAPI\Client\Model\PutRoutingsPlan**](PutRoutingsPlan.md) |  | [optional]
**ivr** | [**\OpenAPI\Client\Model\PutRoutingsIvr**](PutRoutingsIvr.md) |  | [optional]
**queue** | [**\OpenAPI\Client\Model\PutRoutingsQueue**](PutRoutingsQueue.md) |  | [optional]
**api** | [**\OpenAPI\Client\Model\PutRoutingsApi**](PutRoutingsApi.md) |  | [optional]
**mailbox** | [**\OpenAPI\Client\Model\PutRoutingsMailbox**](PutRoutingsMailbox.md) |  | [optional]
**notification** | [**\OpenAPI\Client\Model\PutRoutingsNotification**](PutRoutingsNotification.md) |  | [optional]
**time_settings** | [**\OpenAPI\Client\Model\PutRoutingsTimeSettings**](PutRoutingsTimeSettings.md) |  | [optional]
**custom_callerid** | [**\OpenAPI\Client\Model\PutRoutingsCustomCallerid**](PutRoutingsCustomCallerid.md) |  | [optional]
**music_on_hold** | **bool** |  | [optional]
**notify_external_api** | **bool** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
