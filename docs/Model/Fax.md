# # Fax

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**type** | **string** |  | [optional]
**from_number** | **string** |  | [optional]
**to_number** | **string** |  | [optional]
**pages** | **int** |  | [optional]
**retries** | **int** |  | [optional]
**status** | **string** |  | [optional]
**status_info** | **string** | only populated for outbound faxes | [optional]
**email** | **string** |  | [optional]
**header** | **string** |  | [optional]
**duration** | **int** |  | [optional]
**file** | **string** |  | [optional]
**report** | **string** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
