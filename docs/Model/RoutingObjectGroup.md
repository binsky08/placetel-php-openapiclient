# # RoutingObjectGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**group_ringing_time** | **string** |  | [optional]
**backup_behaviour** | **string** |  | [optional]
**backup_routing_object** | **string** |  | [optional]
**backup_routing_object_id** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
