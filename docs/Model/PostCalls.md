# # PostCalls

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sipuid** | **string** | Sipuid to use (e.g. 7771234567@fpbx.de) |
**target** | **string** | Number to call |
**from_name** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
