# # PutRoutingsApi

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_retries** | **int** |  |
**backup_routing_plan_id** | **int** |  | [optional]
**prompt_id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
