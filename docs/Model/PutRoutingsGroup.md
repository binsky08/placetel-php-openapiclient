# # PutRoutingsGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  |
**group_ringing_time** | **string** |  | [optional] [default to '40']
**backup_behaviour** | **string** |  | [optional]
**backup_routing_object_id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
