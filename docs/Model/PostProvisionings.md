# # PostProvisionings

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  |
**mac** | **string** |  |
**phone_model** | **int** |  |
**webuser_id** | **int** |  | [optional]
**codec** | **string** |  | [optional]
**call_waiting** | **bool** |  | [optional]
**encryption** | **string** |  | [optional]
**locale** | **string** |  | [optional]
**lines** | **string[]** |  | [optional]
**keys** | [**\OpenAPI\Client\Model\PutProvisioningsKeysInner[]**](PutProvisioningsKeysInner.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
