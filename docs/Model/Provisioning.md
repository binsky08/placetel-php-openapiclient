# # Provisioning

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**name** | **string** |  | [optional]
**webuser** | [**\OpenAPI\Client\Model\User**](User.md) |  | [optional]
**mac** | **string** |  | [optional]
**call_waiting** | **string** |  | [optional]
**codec** | **string** |  | [optional]
**locale** | **string** |  | [optional]
**lines** | [**\OpenAPI\Client\Model\Line**](Line.md) |  | [optional]
**keys** | [**\OpenAPI\Client\Model\Key**](Key.md) |  | [optional]
**custom_configurations** | [**\OpenAPI\Client\Model\CustomConfiguration**](CustomConfiguration.md) |  | [optional]
**device** | [**\OpenAPI\Client\Model\Device**](Device.md) |  | [optional]
**encryption** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
