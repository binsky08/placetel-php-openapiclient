# # CallCenterAgent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional]
**name** | **string** |  | [optional]
**description** | **string** |  | [optional]
**extension_sip** | **string** |  | [optional]
**extension_pstn** | **string** |  | [optional]
**extension_type** | **string** |  | [optional]
**agent_type** | **string** |  | [optional]
**status** | **string** |  | [optional]
**priority** | **int** |  | [optional]
**last_call_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
