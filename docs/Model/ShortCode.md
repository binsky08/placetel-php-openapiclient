# # ShortCode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**description** | **string** |  | [optional]
**code** | **string** |  | [optional]
**action** | **string** |  | [optional]
**number** | **string** |  | [optional]
**group_id** | **string** |  | [optional]
**updated_at** | **\DateTime** |  | [optional]
**created_at** | **\DateTime** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
