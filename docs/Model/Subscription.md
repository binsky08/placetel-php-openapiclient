# # Subscription

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**url** | **string** |  | [optional]
**incoming** | **bool** |  | [optional]
**outgoing** | **bool** |  | [optional]
**hungup** | **bool** |  | [optional]
**accepted** | **bool** |  | [optional]
**phone** | **bool** |  | [optional]
**numbers** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
