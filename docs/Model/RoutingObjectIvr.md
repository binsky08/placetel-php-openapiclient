# # RoutingObjectIvr

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prompt** | [**\OpenAPI\Client\Model\Prompt**](Prompt.md) |  | [optional]
**items** | **string** |  | [optional]
**item_ids** | **string** |  | [optional]
**catchall_routing_object** | **string** |  | [optional]
**catchall_routing_object_id** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
