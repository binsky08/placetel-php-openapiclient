# # RoutingObjectMailbox

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **string** |  | [optional]
**mp3** | **string** |  | [optional]
**greeting** | [**\OpenAPI\Client\Model\Prompt**](Prompt.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
