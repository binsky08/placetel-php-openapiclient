# # PutRoutingsIvr

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**prompt_id** | **int** |  | [optional]
**item_1_id** | **int** |  | [optional]
**item_2_id** | **int** |  | [optional]
**item_3_id** | **int** |  | [optional]
**item_4_id** | **int** |  | [optional]
**item_5_id** | **int** |  | [optional]
**item_6_id** | **int** |  | [optional]
**item_7_id** | **int** |  | [optional]
**item_8_id** | **int** |  | [optional]
**item_9_id** | **int** |  | [optional]
**catchall_routing_object_id** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
