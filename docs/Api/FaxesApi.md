# OpenAPI\Client\FaxesApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteFaxesId()**](FaxesApi.md#deleteFaxesId) | **DELETE** /faxes/{id} | Delete a fax |
| [**getFaxes()**](FaxesApi.md#getFaxes) | **GET** /faxes | Fetch in- and outbound faxes |
| [**getFaxesId()**](FaxesApi.md#getFaxesId) | **GET** /faxes/{id} | Fetch specific fax |
| [**postFaxes()**](FaxesApi.md#postFaxes) | **POST** /faxes | Create outbound fax |


## `deleteFaxesId()`

```php
deleteFaxesId($id)
```

Delete a fax

Delete a fax by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\FaxesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Fax ID

try {
    $apiInstance->deleteFaxesId($id);
} catch (Exception $e) {
    echo 'Exception when calling FaxesApi->deleteFaxesId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Fax ID | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getFaxes()`

```php
getFaxes($filter_type, $filter_from_number, $filter_to_number, $order, $page, $per_page): \OpenAPI\Client\Model\Fax[]
```

Fetch in- and outbound faxes

Provides a list of all faxes

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\FaxesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter_type = 'inbound,outbound'; // string
$filter_from_number = 'filter_from_number_example'; // string
$filter_to_number = 'filter_to_number_example'; // string
$order = 'desc'; // string
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getFaxes($filter_type, $filter_from_number, $filter_to_number, $order, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FaxesApi->getFaxes: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **filter_type** | **string**|  | [optional] [default to &#39;inbound,outbound&#39;] |
| **filter_from_number** | **string**|  | [optional] |
| **filter_to_number** | **string**|  | [optional] |
| **order** | **string**|  | [optional] [default to &#39;desc&#39;] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\Fax[]**](../Model/Fax.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getFaxesId()`

```php
getFaxesId($id): \OpenAPI\Client\Model\Fax
```

Fetch specific fax

Fetch in- or outbound fax

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\FaxesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Fax ID

try {
    $result = $apiInstance->getFaxesId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FaxesApi->getFaxesId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Fax ID | |

### Return type

[**\OpenAPI\Client\Model\Fax**](../Model/Fax.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postFaxes()`

```php
postFaxes($faxes): \OpenAPI\Client\Model\Fax
```

Create outbound fax

Sends faxes via API call

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\FaxesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$faxes = new \OpenAPI\Client\Model\PostFaxes(); // \OpenAPI\Client\Model\PostFaxes

try {
    $result = $apiInstance->postFaxes($faxes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FaxesApi->postFaxes: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **faxes** | [**\OpenAPI\Client\Model\PostFaxes**](../Model/PostFaxes.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Fax**](../Model/Fax.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
