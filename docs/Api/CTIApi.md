# OpenAPI\Client\CTIApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**getCtiMac()**](CTIApi.md#getCtiMac) | **GET** /cti/{mac} | Get config params |
| [**postCtiMacAnswer()**](CTIApi.md#postCtiMacAnswer) | **POST** /cti/{mac}/answer | Answer |
| [**postCtiMacBlindTransfer()**](CTIApi.md#postCtiMacBlindTransfer) | **POST** /cti/{mac}/blind_transfer | Blind transfer |
| [**postCtiMacCompleteConference()**](CTIApi.md#postCtiMacCompleteConference) | **POST** /cti/{mac}/complete_conference | Complete conference |
| [**postCtiMacCompleteTransfer()**](CTIApi.md#postCtiMacCompleteTransfer) | **POST** /cti/{mac}/complete_transfer | Complete transfer |
| [**postCtiMacDecline()**](CTIApi.md#postCtiMacDecline) | **POST** /cti/{mac}/decline | Decline |
| [**postCtiMacDial()**](CTIApi.md#postCtiMacDial) | **POST** /cti/{mac}/dial | Dial |
| [**postCtiMacDialDigit()**](CTIApi.md#postCtiMacDialDigit) | **POST** /cti/{mac}/dial_digit | Dial digit |
| [**postCtiMacHangup()**](CTIApi.md#postCtiMacHangup) | **POST** /cti/{mac}/hangup | Hangup |
| [**postCtiMacHold()**](CTIApi.md#postCtiMacHold) | **POST** /cti/{mac}/hold | Hold |
| [**postCtiMacResume()**](CTIApi.md#postCtiMacResume) | **POST** /cti/{mac}/resume | Resume |
| [**postCtiMacSendDtmfDigits()**](CTIApi.md#postCtiMacSendDtmfDigits) | **POST** /cti/{mac}/send_dtmf_digits | Send DTMF Digits |
| [**postCtiMacStartConference()**](CTIApi.md#postCtiMacStartConference) | **POST** /cti/{mac}/start_conference | Start conference |
| [**postCtiMacStartTransfer()**](CTIApi.md#postCtiMacStartTransfer) | **POST** /cti/{mac}/start_transfer | Start transfer |
| [**putCtiMac()**](CTIApi.md#putCtiMac) | **PUT** /cti/{mac} | Set config params |


## `getCtiMac()`

```php
getCtiMac($params, $mac)
```

Get config params

Get config params

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$params = array('params_example'); // string[]
$mac = 56; // int

try {
    $apiInstance->getCtiMac($params, $mac);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->getCtiMac: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **params** | [**string[]**](../Model/string.md)|  | |
| **mac** | **int**|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacAnswer()`

```php
postCtiMacAnswer($mac, $cti_mac_answer)
```

Answer

This method allows device to answer a ringing call with given call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_answer = new \OpenAPI\Client\Model\PostCtiMacAnswer(); // \OpenAPI\Client\Model\PostCtiMacAnswer

try {
    $apiInstance->postCtiMacAnswer($mac, $cti_mac_answer);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacAnswer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_answer** | [**\OpenAPI\Client\Model\PostCtiMacAnswer**](../Model/PostCtiMacAnswer.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacBlindTransfer()`

```php
postCtiMacBlindTransfer($mac, $cti_mac_blind_transfer)
```

Blind transfer

This method allows to do a blind tranfser on the call specified by the call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_blind_transfer = new \OpenAPI\Client\Model\PostCtiMacBlindTransfer(); // \OpenAPI\Client\Model\PostCtiMacBlindTransfer

try {
    $apiInstance->postCtiMacBlindTransfer($mac, $cti_mac_blind_transfer);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacBlindTransfer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_blind_transfer** | [**\OpenAPI\Client\Model\PostCtiMacBlindTransfer**](../Model/PostCtiMacBlindTransfer.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacCompleteConference()`

```php
postCtiMacCompleteConference($mac, $cti_mac_complete_conference)
```

Complete conference

This method allows to complete conference on the call specified by the call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_complete_conference = new \OpenAPI\Client\Model\PostCtiMacCompleteConference(); // \OpenAPI\Client\Model\PostCtiMacCompleteConference

try {
    $apiInstance->postCtiMacCompleteConference($mac, $cti_mac_complete_conference);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacCompleteConference: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_complete_conference** | [**\OpenAPI\Client\Model\PostCtiMacCompleteConference**](../Model/PostCtiMacCompleteConference.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacCompleteTransfer()`

```php
postCtiMacCompleteTransfer($mac, $cti_mac_complete_transfer)
```

Complete transfer

This method allows to initiate the tranfser on the call specified by the call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_complete_transfer = new \OpenAPI\Client\Model\PostCtiMacCompleteTransfer(); // \OpenAPI\Client\Model\PostCtiMacCompleteTransfer

try {
    $apiInstance->postCtiMacCompleteTransfer($mac, $cti_mac_complete_transfer);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacCompleteTransfer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_complete_transfer** | [**\OpenAPI\Client\Model\PostCtiMacCompleteTransfer**](../Model/PostCtiMacCompleteTransfer.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacDecline()`

```php
postCtiMacDecline($mac, $cti_mac_decline)
```

Decline

This method allows device to Decline the call specified by the call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_decline = new \OpenAPI\Client\Model\PostCtiMacDecline(); // \OpenAPI\Client\Model\PostCtiMacDecline

try {
    $apiInstance->postCtiMacDecline($mac, $cti_mac_decline);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacDecline: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_decline** | [**\OpenAPI\Client\Model\PostCtiMacDecline**](../Model/PostCtiMacDecline.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacDial()`

```php
postCtiMacDial($mac, $cti_mac_dial)
```

Dial

This method initiates a new call.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_dial = new \OpenAPI\Client\Model\PostCtiMacDial(); // \OpenAPI\Client\Model\PostCtiMacDial

try {
    $apiInstance->postCtiMacDial($mac, $cti_mac_dial);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacDial: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_dial** | [**\OpenAPI\Client\Model\PostCtiMacDial**](../Model/PostCtiMacDial.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacDialDigit()`

```php
postCtiMacDialDigit($mac, $cti_mac_dial_digit)
```

Dial digit

This method initiates a new call.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_dial_digit = new \OpenAPI\Client\Model\PostCtiMacDialDigit(); // \OpenAPI\Client\Model\PostCtiMacDialDigit

try {
    $apiInstance->postCtiMacDialDigit($mac, $cti_mac_dial_digit);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacDialDigit: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_dial_digit** | [**\OpenAPI\Client\Model\PostCtiMacDialDigit**](../Model/PostCtiMacDialDigit.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacHangup()`

```php
postCtiMacHangup($mac, $cti_mac_hangup)
```

Hangup

This method allows device to end the call specified by the call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_hangup = new \OpenAPI\Client\Model\PostCtiMacHangup(); // \OpenAPI\Client\Model\PostCtiMacHangup

try {
    $apiInstance->postCtiMacHangup($mac, $cti_mac_hangup);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacHangup: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_hangup** | [**\OpenAPI\Client\Model\PostCtiMacHangup**](../Model/PostCtiMacHangup.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacHold()`

```php
postCtiMacHold($mac, $cti_mac_hold)
```

Hold

This method allows device to hold the call specified by the call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_hold = new \OpenAPI\Client\Model\PostCtiMacHold(); // \OpenAPI\Client\Model\PostCtiMacHold

try {
    $apiInstance->postCtiMacHold($mac, $cti_mac_hold);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacHold: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_hold** | [**\OpenAPI\Client\Model\PostCtiMacHold**](../Model/PostCtiMacHold.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacResume()`

```php
postCtiMacResume($mac, $cti_mac_resume)
```

Resume

This method allows device to resume the call specified by the call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_resume = new \OpenAPI\Client\Model\PostCtiMacResume(); // \OpenAPI\Client\Model\PostCtiMacResume

try {
    $apiInstance->postCtiMacResume($mac, $cti_mac_resume);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacResume: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_resume** | [**\OpenAPI\Client\Model\PostCtiMacResume**](../Model/PostCtiMacResume.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacSendDtmfDigits()`

```php
postCtiMacSendDtmfDigits($mac, $cti_mac_send_dtmf_digits)
```

Send DTMF Digits

This method allows to sendDTMFDigits on a current active call.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_send_dtmf_digits = new \OpenAPI\Client\Model\PostCtiMacSendDtmfDigits(); // \OpenAPI\Client\Model\PostCtiMacSendDtmfDigits

try {
    $apiInstance->postCtiMacSendDtmfDigits($mac, $cti_mac_send_dtmf_digits);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacSendDtmfDigits: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_send_dtmf_digits** | [**\OpenAPI\Client\Model\PostCtiMacSendDtmfDigits**](../Model/PostCtiMacSendDtmfDigits.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacStartConference()`

```php
postCtiMacStartConference($mac, $cti_mac_start_conference)
```

Start conference

This method allows to initiate conference on the call specified by the call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_start_conference = new \OpenAPI\Client\Model\PostCtiMacStartConference(); // \OpenAPI\Client\Model\PostCtiMacStartConference

try {
    $apiInstance->postCtiMacStartConference($mac, $cti_mac_start_conference);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacStartConference: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_start_conference** | [**\OpenAPI\Client\Model\PostCtiMacStartConference**](../Model/PostCtiMacStartConference.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCtiMacStartTransfer()`

```php
postCtiMacStartTransfer($mac, $cti_mac_start_transfer)
```

Start transfer

This method allows to initiate the tranfser on the call specified by the call_id.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti_mac_start_transfer = new \OpenAPI\Client\Model\PostCtiMacStartTransfer(); // \OpenAPI\Client\Model\PostCtiMacStartTransfer

try {
    $apiInstance->postCtiMacStartTransfer($mac, $cti_mac_start_transfer);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->postCtiMacStartTransfer: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti_mac_start_transfer** | [**\OpenAPI\Client\Model\PostCtiMacStartTransfer**](../Model/PostCtiMacStartTransfer.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putCtiMac()`

```php
putCtiMac($mac, $cti)
```

Set config params

Set config params

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CTIApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$mac = 56; // int
$cti = new \OpenAPI\Client\Model\PutCti(); // \OpenAPI\Client\Model\PutCti

try {
    $apiInstance->putCtiMac($mac, $cti);
} catch (Exception $e) {
    echo 'Exception when calling CTIApi->putCtiMac: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **mac** | **int**|  | |
| **cti** | [**\OpenAPI\Client\Model\PutCti**](../Model/PutCti.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
