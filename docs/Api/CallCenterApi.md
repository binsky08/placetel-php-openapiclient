# OpenAPI\Client\CallCenterApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**getCallCenterAgents()**](CallCenterApi.md#getCallCenterAgents) | **GET** /call_center_agents | Fetch all call center agents |
| [**getCallCenterCalls()**](CallCenterApi.md#getCallCenterCalls) | **GET** /call_center_calls | Fetch all call center calls |
| [**getCallCenterQueues()**](CallCenterApi.md#getCallCenterQueues) | **GET** /call_center_queues | Fetch all call center queues |
| [**postCallCenterAgents()**](CallCenterApi.md#postCallCenterAgents) | **POST** /call_center_agents | Create an agent |
| [**postCallCenterQueues()**](CallCenterApi.md#postCallCenterQueues) | **POST** /call_center_queues | Create a queue |
| [**putCallCenterAgentsId()**](CallCenterApi.md#putCallCenterAgentsId) | **PUT** /call_center_agents/{id} | Update an agent |
| [**putCallCenterQueuesId()**](CallCenterApi.md#putCallCenterQueuesId) | **PUT** /call_center_queues/{id} | Update a queue |


## `getCallCenterAgents()`

```php
getCallCenterAgents($page, $per_page): \OpenAPI\Client\Model\CallCenterAgent[]
```

Fetch all call center agents

Provides a list of all call center agents

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallCenterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getCallCenterAgents($page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallCenterApi->getCallCenterAgents: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\CallCenterAgent[]**](../Model/CallCenterAgent.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getCallCenterCalls()`

```php
getCallCenterCalls($date, $queue_ids, $page, $per_page): \OpenAPI\Client\Model\CallCenterCall[]
```

Fetch all call center calls

Provides a list of all call center calls

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallCenterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | YYYY-MM-DD
$queue_ids = array(56); // int[] | 1
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getCallCenterCalls($date, $queue_ids, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallCenterApi->getCallCenterCalls: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **date** | **\DateTime**| YYYY-MM-DD | |
| **queue_ids** | [**int[]**](../Model/int.md)| 1 | [optional] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\CallCenterCall[]**](../Model/CallCenterCall.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getCallCenterQueues()`

```php
getCallCenterQueues($page, $per_page): \OpenAPI\Client\Model\CallCenterQueue[]
```

Fetch all call center queues

Provides a list of all call center queues

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallCenterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getCallCenterQueues($page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallCenterApi->getCallCenterQueues: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\CallCenterQueue[]**](../Model/CallCenterQueue.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCallCenterAgents()`

```php
postCallCenterAgents($call_center_agents): \OpenAPI\Client\Model\CallCenterAgent
```

Create an agent

Create a new agent

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallCenterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$call_center_agents = new \OpenAPI\Client\Model\PostCallCenterAgents(); // \OpenAPI\Client\Model\PostCallCenterAgents

try {
    $result = $apiInstance->postCallCenterAgents($call_center_agents);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallCenterApi->postCallCenterAgents: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **call_center_agents** | [**\OpenAPI\Client\Model\PostCallCenterAgents**](../Model/PostCallCenterAgents.md)|  | |

### Return type

[**\OpenAPI\Client\Model\CallCenterAgent**](../Model/CallCenterAgent.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCallCenterQueues()`

```php
postCallCenterQueues($call_center_queues): \OpenAPI\Client\Model\CallCenterQueue
```

Create a queue

Create a queue

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallCenterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$call_center_queues = new \OpenAPI\Client\Model\PostCallCenterQueues(); // \OpenAPI\Client\Model\PostCallCenterQueues

try {
    $result = $apiInstance->postCallCenterQueues($call_center_queues);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallCenterApi->postCallCenterQueues: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **call_center_queues** | [**\OpenAPI\Client\Model\PostCallCenterQueues**](../Model/PostCallCenterQueues.md)|  | |

### Return type

[**\OpenAPI\Client\Model\CallCenterQueue**](../Model/CallCenterQueue.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putCallCenterAgentsId()`

```php
putCallCenterAgentsId($id, $call_center_agents): \OpenAPI\Client\Model\CallCenterAgent
```

Update an agent

Update an agent by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallCenterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Sip user ID
$call_center_agents = new \OpenAPI\Client\Model\PutCallCenterAgents(); // \OpenAPI\Client\Model\PutCallCenterAgents

try {
    $result = $apiInstance->putCallCenterAgentsId($id, $call_center_agents);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallCenterApi->putCallCenterAgentsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Sip user ID | |
| **call_center_agents** | [**\OpenAPI\Client\Model\PutCallCenterAgents**](../Model/PutCallCenterAgents.md)|  | |

### Return type

[**\OpenAPI\Client\Model\CallCenterAgent**](../Model/CallCenterAgent.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putCallCenterQueuesId()`

```php
putCallCenterQueuesId($id, $call_center_queues): \OpenAPI\Client\Model\CallCenterQueue
```

Update a queue

Update a queue

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallCenterApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Queue ID
$call_center_queues = new \OpenAPI\Client\Model\PutCallCenterQueues(); // \OpenAPI\Client\Model\PutCallCenterQueues

try {
    $result = $apiInstance->putCallCenterQueuesId($id, $call_center_queues);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallCenterApi->putCallCenterQueuesId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Queue ID | |
| **call_center_queues** | [**\OpenAPI\Client\Model\PutCallCenterQueues**](../Model/PutCallCenterQueues.md)|  | |

### Return type

[**\OpenAPI\Client\Model\CallCenterQueue**](../Model/CallCenterQueue.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
