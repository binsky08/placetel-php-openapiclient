# OpenAPI\Client\ContactsApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteContactsId()**](ContactsApi.md#deleteContactsId) | **DELETE** /contacts/{id} | Deletes a contact |
| [**getContacts()**](ContactsApi.md#getContacts) | **GET** /contacts | Get all contacts |
| [**getContactsId()**](ContactsApi.md#getContactsId) | **GET** /contacts/{id} | Retrieve a contact |
| [**postContacts()**](ContactsApi.md#postContacts) | **POST** /contacts | Create a contact |
| [**putContactsId()**](ContactsApi.md#putContactsId) | **PUT** /contacts/{id} | Update a contact |


## `deleteContactsId()`

```php
deleteContactsId($id)
```

Deletes a contact

Deletes a contact by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Contact ID

try {
    $apiInstance->deleteContactsId($id);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->deleteContactsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Contact ID | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getContacts()`

```php
getContacts($filter_speeddial, $filter_first_name, $filter_last_name, $filter_company, $filter_email, $filter_email_work, $filter_phone_work, $filter_mobile_work, $filter_phone, $filter_mobile, $filter_blocked, $search_number, $page, $per_page): \OpenAPI\Client\Model\Contact[]
```

Get all contacts

Provides a list of all contacts

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter_speeddial = 56; // int
$filter_first_name = 'filter_first_name_example'; // string
$filter_last_name = 'filter_last_name_example'; // string
$filter_company = 'filter_company_example'; // string
$filter_email = 'filter_email_example'; // string
$filter_email_work = 'filter_email_work_example'; // string
$filter_phone_work = 'filter_phone_work_example'; // string
$filter_mobile_work = 'filter_mobile_work_example'; // string
$filter_phone = 'filter_phone_example'; // string
$filter_mobile = 'filter_mobile_example'; // string
$filter_blocked = false; // bool
$search_number = 'search_number_example'; // string
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getContacts($filter_speeddial, $filter_first_name, $filter_last_name, $filter_company, $filter_email, $filter_email_work, $filter_phone_work, $filter_mobile_work, $filter_phone, $filter_mobile, $filter_blocked, $search_number, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->getContacts: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **filter_speeddial** | **int**|  | [optional] |
| **filter_first_name** | **string**|  | [optional] |
| **filter_last_name** | **string**|  | [optional] |
| **filter_company** | **string**|  | [optional] |
| **filter_email** | **string**|  | [optional] |
| **filter_email_work** | **string**|  | [optional] |
| **filter_phone_work** | **string**|  | [optional] |
| **filter_mobile_work** | **string**|  | [optional] |
| **filter_phone** | **string**|  | [optional] |
| **filter_mobile** | **string**|  | [optional] |
| **filter_blocked** | **bool**|  | [optional] [default to false] |
| **search_number** | **string**|  | [optional] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\Contact[]**](../Model/Contact.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getContactsId()`

```php
getContactsId($id): \OpenAPI\Client\Model\Contact
```

Retrieve a contact

Fetches a contact by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | ID of a contact

try {
    $result = $apiInstance->getContactsId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->getContactsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| ID of a contact | |

### Return type

[**\OpenAPI\Client\Model\Contact**](../Model/Contact.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postContacts()`

```php
postContacts($contacts): \OpenAPI\Client\Model\Contact
```

Create a contact

Creates a contact

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$contacts = new \OpenAPI\Client\Model\PostContacts(); // \OpenAPI\Client\Model\PostContacts

try {
    $result = $apiInstance->postContacts($contacts);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->postContacts: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **contacts** | [**\OpenAPI\Client\Model\PostContacts**](../Model/PostContacts.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Contact**](../Model/Contact.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putContactsId()`

```php
putContactsId($id, $contacts): \OpenAPI\Client\Model\Contact
```

Update a contact

Updates a contact for a given ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ContactsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Contact ID
$contacts = new \OpenAPI\Client\Model\PutContacts(); // \OpenAPI\Client\Model\PutContacts

try {
    $result = $apiInstance->putContactsId($id, $contacts);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ContactsApi->putContactsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Contact ID | |
| **contacts** | [**\OpenAPI\Client\Model\PutContacts**](../Model/PutContacts.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Contact**](../Model/Contact.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
