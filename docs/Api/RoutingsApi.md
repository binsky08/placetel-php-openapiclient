# OpenAPI\Client\RoutingsApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteRoutingsNumberOrId()**](RoutingsApi.md#deleteRoutingsNumberOrId) | **DELETE** /routings/{number_or_id} | Delete a routing object |
| [**getRoutings()**](RoutingsApi.md#getRoutings) | **GET** /routings | Get all routings |
| [**getRoutingsNumberOrId()**](RoutingsApi.md#getRoutingsNumberOrId) | **GET** /routings/{number_or_id} | Retrieve routing |
| [**postRoutings()**](RoutingsApi.md#postRoutings) | **POST** /routings | Create a routing object |
| [**putRoutingsNumberOrId()**](RoutingsApi.md#putRoutingsNumberOrId) | **PUT** /routings/{number_or_id} | Update routing |


## `deleteRoutingsNumberOrId()`

```php
deleteRoutingsNumberOrId($number_or_id): \OpenAPI\Client\Model\RoutingObject
```

Delete a routing object

Delete a routing object

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$number_or_id = 'number_or_id_example'; // string | A number or an ID

try {
    $result = $apiInstance->deleteRoutingsNumberOrId($number_or_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingsApi->deleteRoutingsNumberOrId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **number_or_id** | **string**| A number or an ID | |

### Return type

[**\OpenAPI\Client\Model\RoutingObject**](../Model/RoutingObject.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getRoutings()`

```php
getRoutings($filter_updated_at_gte, $filter_updated_at_lte, $page, $per_page): \OpenAPI\Client\Model\RoutingObject[]
```

Get all routings

Provides a list of routing

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter_updated_at_gte = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime
$filter_updated_at_lte = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getRoutings($filter_updated_at_gte, $filter_updated_at_lte, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingsApi->getRoutings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **filter_updated_at_gte** | **\DateTime**|  | [optional] |
| **filter_updated_at_lte** | **\DateTime**|  | [optional] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\RoutingObject[]**](../Model/RoutingObject.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getRoutingsNumberOrId()`

```php
getRoutingsNumberOrId($number_or_id): \OpenAPI\Client\Model\RoutingObject
```

Retrieve routing

Fetches a routing

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$number_or_id = 'number_or_id_example'; // string | A number or an ID

try {
    $result = $apiInstance->getRoutingsNumberOrId($number_or_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingsApi->getRoutingsNumberOrId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **number_or_id** | **string**| A number or an ID | |

### Return type

[**\OpenAPI\Client\Model\RoutingObject**](../Model/RoutingObject.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postRoutings()`

```php
postRoutings($routings): \OpenAPI\Client\Model\RoutingObject
```

Create a routing object

Create a routing object. Routing can be created as: `forward`, `group`, `ivr`, `queue` or `api`

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$routings = new \OpenAPI\Client\Model\PostRoutings(); // \OpenAPI\Client\Model\PostRoutings

try {
    $result = $apiInstance->postRoutings($routings);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingsApi->postRoutings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **routings** | [**\OpenAPI\Client\Model\PostRoutings**](../Model/PostRoutings.md)|  | |

### Return type

[**\OpenAPI\Client\Model\RoutingObject**](../Model/RoutingObject.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putRoutingsNumberOrId()`

```php
putRoutingsNumberOrId($number_or_id, $routings): \OpenAPI\Client\Model\RoutingObject
```

Update routing

Updates the routing object. Routing can be set to: `forward`, `group`, `plan`, `ivr`, `queue` or `api`

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$number_or_id = 'number_or_id_example'; // string | A number or an ID
$routings = new \OpenAPI\Client\Model\PutRoutings(); // \OpenAPI\Client\Model\PutRoutings

try {
    $result = $apiInstance->putRoutingsNumberOrId($number_or_id, $routings);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingsApi->putRoutingsNumberOrId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **number_or_id** | **string**| A number or an ID | |
| **routings** | [**\OpenAPI\Client\Model\PutRoutings**](../Model/PutRoutings.md)|  | |

### Return type

[**\OpenAPI\Client\Model\RoutingObject**](../Model/RoutingObject.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
