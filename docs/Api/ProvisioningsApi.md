# OpenAPI\Client\ProvisioningsApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteProvisioningsId()**](ProvisioningsApi.md#deleteProvisioningsId) | **DELETE** /provisionings/{id} | Delete a phone provisionings |
| [**deleteProvisioningsIdCustomConfigurationsCustomConfigurationId()**](ProvisioningsApi.md#deleteProvisioningsIdCustomConfigurationsCustomConfigurationId) | **DELETE** /provisionings/{id}/custom_configurations/{custom_configuration_id} | Delete a phone provisionings custom configuration key |
| [**getProvisionings()**](ProvisioningsApi.md#getProvisionings) | **GET** /provisionings | Get all phone provisionings |
| [**getProvisioningsId()**](ProvisioningsApi.md#getProvisioningsId) | **GET** /provisionings/{id} | Retrieve a phone provisionings |
| [**getProvisioningsIdCustomConfigurations()**](ProvisioningsApi.md#getProvisioningsIdCustomConfigurations) | **GET** /provisionings/{id}/custom_configurations | Retrieve a phone provisionings custom configurations |
| [**postProvisionings()**](ProvisioningsApi.md#postProvisionings) | **POST** /provisionings | Creates a phone provisionings |
| [**postProvisioningsIdReconfigure()**](ProvisioningsApi.md#postProvisioningsIdReconfigure) | **POST** /provisionings/{id}/reconfigure | Trigger reconfiguration of a phone |
| [**putProvisioningsId()**](ProvisioningsApi.md#putProvisioningsId) | **PUT** /provisionings/{id} | Update a phone provisionings |
| [**putProvisioningsIdCustomConfigurations()**](ProvisioningsApi.md#putProvisioningsIdCustomConfigurations) | **PUT** /provisionings/{id}/custom_configurations | Update or create a phone provisionings custom configuration key |


## `deleteProvisioningsId()`

```php
deleteProvisioningsId($id)
```

Delete a phone provisionings

Delete a phone provisioning

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ProvisioningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | ID

try {
    $apiInstance->deleteProvisioningsId($id);
} catch (Exception $e) {
    echo 'Exception when calling ProvisioningsApi->deleteProvisioningsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| ID | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteProvisioningsIdCustomConfigurationsCustomConfigurationId()`

```php
deleteProvisioningsIdCustomConfigurationsCustomConfigurationId($id, $custom_configuration_id)
```

Delete a phone provisionings custom configuration key

Delete a phone provisioning custom configuration key

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ProvisioningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | ID
$custom_configuration_id = 56; // int | Custom configuration ID

try {
    $apiInstance->deleteProvisioningsIdCustomConfigurationsCustomConfigurationId($id, $custom_configuration_id);
} catch (Exception $e) {
    echo 'Exception when calling ProvisioningsApi->deleteProvisioningsIdCustomConfigurationsCustomConfigurationId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| ID | |
| **custom_configuration_id** | **int**| Custom configuration ID | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getProvisionings()`

```php
getProvisionings($search, $page, $per_page): \OpenAPI\Client\Model\Provisioning[]
```

Get all phone provisionings

Provides a full list of all registered provisioned phones

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ProvisioningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$search = 'search_example'; // string
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getProvisionings($search, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProvisioningsApi->getProvisionings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **search** | **string**|  | [optional] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\Provisioning[]**](../Model/Provisioning.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getProvisioningsId()`

```php
getProvisioningsId($id): \OpenAPI\Client\Model\Provisioning
```

Retrieve a phone provisionings

Fetches a phone provisioning by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ProvisioningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | ID

try {
    $result = $apiInstance->getProvisioningsId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProvisioningsApi->getProvisioningsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| ID | |

### Return type

[**\OpenAPI\Client\Model\Provisioning**](../Model/Provisioning.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getProvisioningsIdCustomConfigurations()`

```php
getProvisioningsIdCustomConfigurations($id): \OpenAPI\Client\Model\CustomConfiguration
```

Retrieve a phone provisionings custom configurations

Fetches a phone provisionings custom configurations by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ProvisioningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | ID

try {
    $result = $apiInstance->getProvisioningsIdCustomConfigurations($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProvisioningsApi->getProvisioningsIdCustomConfigurations: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| ID | |

### Return type

[**\OpenAPI\Client\Model\CustomConfiguration**](../Model/CustomConfiguration.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postProvisionings()`

```php
postProvisionings($provisionings): \OpenAPI\Client\Model\Provisioning
```

Creates a phone provisionings

Creates a phone provisioning

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ProvisioningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$provisionings = new \OpenAPI\Client\Model\PostProvisionings(); // \OpenAPI\Client\Model\PostProvisionings

try {
    $result = $apiInstance->postProvisionings($provisionings);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProvisioningsApi->postProvisionings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **provisionings** | [**\OpenAPI\Client\Model\PostProvisionings**](../Model/PostProvisionings.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Provisioning**](../Model/Provisioning.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postProvisioningsIdReconfigure()`

```php
postProvisioningsIdReconfigure($id): \OpenAPI\Client\Model\Provisioning
```

Trigger reconfiguration of a phone

This sends a SIP command to the user on line 1 to fetch configuration changes.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ProvisioningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | ID

try {
    $result = $apiInstance->postProvisioningsIdReconfigure($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProvisioningsApi->postProvisioningsIdReconfigure: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| ID | |

### Return type

[**\OpenAPI\Client\Model\Provisioning**](../Model/Provisioning.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putProvisioningsId()`

```php
putProvisioningsId($id, $provisionings): \OpenAPI\Client\Model\Provisioning
```

Update a phone provisionings

Updates a phone provisioning

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ProvisioningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | ID
$provisionings = new \OpenAPI\Client\Model\PutProvisionings(); // \OpenAPI\Client\Model\PutProvisionings

try {
    $result = $apiInstance->putProvisioningsId($id, $provisionings);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProvisioningsApi->putProvisioningsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| ID | |
| **provisionings** | [**\OpenAPI\Client\Model\PutProvisionings**](../Model/PutProvisionings.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Provisioning**](../Model/Provisioning.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putProvisioningsIdCustomConfigurations()`

```php
putProvisioningsIdCustomConfigurations($id, $provisionings_id_custom_configurations): \OpenAPI\Client\Model\Provisioning
```

Update or create a phone provisionings custom configuration key

Updates or creates a phone provisioning custom configuration key

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\ProvisioningsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | ID
$provisionings_id_custom_configurations = new \OpenAPI\Client\Model\PutProvisioningsIdCustomConfigurations(); // \OpenAPI\Client\Model\PutProvisioningsIdCustomConfigurations

try {
    $result = $apiInstance->putProvisioningsIdCustomConfigurations($id, $provisionings_id_custom_configurations);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProvisioningsApi->putProvisioningsIdCustomConfigurations: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| ID | |
| **provisionings_id_custom_configurations** | [**\OpenAPI\Client\Model\PutProvisioningsIdCustomConfigurations**](../Model/PutProvisioningsIdCustomConfigurations.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Provisioning**](../Model/Provisioning.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
