# OpenAPI\Client\PromptsApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deletePromptsId()**](PromptsApi.md#deletePromptsId) | **DELETE** /prompts/{id} | Delete a prompt |
| [**getPrompts()**](PromptsApi.md#getPrompts) | **GET** /prompts | Fetch all prompts |
| [**getPromptsId()**](PromptsApi.md#getPromptsId) | **GET** /prompts/{id} | Retrieve a prompt |
| [**postPrompts()**](PromptsApi.md#postPrompts) | **POST** /prompts | Create a prompt |
| [**putPromptsId()**](PromptsApi.md#putPromptsId) | **PUT** /prompts/{id} | Update a prompt |


## `deletePromptsId()`

```php
deletePromptsId($id)
```

Delete a prompt

Delete a prompt by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\PromptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Prompt ID

try {
    $apiInstance->deletePromptsId($id);
} catch (Exception $e) {
    echo 'Exception when calling PromptsApi->deletePromptsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Prompt ID | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getPrompts()`

```php
getPrompts($filter_name, $filter_description, $page, $per_page): \OpenAPI\Client\Model\Prompt[]
```

Fetch all prompts

Provides a list of all prompts

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\PromptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter_name = 'filter_name_example'; // string
$filter_description = 'filter_description_example'; // string
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getPrompts($filter_name, $filter_description, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromptsApi->getPrompts: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **filter_name** | **string**|  | [optional] |
| **filter_description** | **string**|  | [optional] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\Prompt[]**](../Model/Prompt.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getPromptsId()`

```php
getPromptsId($id): \OpenAPI\Client\Model\Prompt
```

Retrieve a prompt

Fetches a prompt by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\PromptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Prompt ID

try {
    $result = $apiInstance->getPromptsId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromptsApi->getPromptsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Prompt ID | |

### Return type

[**\OpenAPI\Client\Model\Prompt**](../Model/Prompt.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postPrompts()`

```php
postPrompts($prompts): \OpenAPI\Client\Model\Prompt
```

Create a prompt

Creates a new prompt

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\PromptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$prompts = new \OpenAPI\Client\Model\PostPrompts(); // \OpenAPI\Client\Model\PostPrompts

try {
    $result = $apiInstance->postPrompts($prompts);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromptsApi->postPrompts: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **prompts** | [**\OpenAPI\Client\Model\PostPrompts**](../Model/PostPrompts.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Prompt**](../Model/Prompt.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putPromptsId()`

```php
putPromptsId($id, $prompts): \OpenAPI\Client\Model\Prompt
```

Update a prompt

Update a prompt by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\PromptsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Prompt ID
$prompts = new \OpenAPI\Client\Model\PutPrompts(); // \OpenAPI\Client\Model\PutPrompts

try {
    $result = $apiInstance->putPromptsId($id, $prompts);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PromptsApi->putPromptsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Prompt ID | |
| **prompts** | [**\OpenAPI\Client\Model\PutPrompts**](../Model/PutPrompts.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Prompt**](../Model/Prompt.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
