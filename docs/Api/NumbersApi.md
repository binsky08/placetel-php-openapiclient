# OpenAPI\Client\NumbersApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteNumbersNumberIdProfilesId()**](NumbersApi.md#deleteNumbersNumberIdProfilesId) | **DELETE** /numbers/{number_id}/profiles/{id} | Delete a profile for a number |
| [**getNumbers()**](NumbersApi.md#getNumbers) | **GET** /numbers | Get all numbers |
| [**getNumbersId()**](NumbersApi.md#getNumbersId) | **GET** /numbers/{id} | Retrieve a number |
| [**getNumbersNumberIdProfiles()**](NumbersApi.md#getNumbersNumberIdProfiles) | **GET** /numbers/{number_id}/profiles | List profiles for a number |
| [**postNumbersNumberIdProfiles()**](NumbersApi.md#postNumbersNumberIdProfiles) | **POST** /numbers/{number_id}/profiles | Creates a new profile for a number |
| [**putNumbersNumberIdProfiles()**](NumbersApi.md#putNumbersNumberIdProfiles) | **PUT** /numbers/{number_id}/profiles | Activates a profile for a number |


## `deleteNumbersNumberIdProfilesId()`

```php
deleteNumbersNumberIdProfilesId($number_id, $id): \OpenAPI\Client\Model\Profile
```

Delete a profile for a number

Removes the profile for a number

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\NumbersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$number_id = 56; // int | Number ID
$id = 56; // int | Profile ID

try {
    $result = $apiInstance->deleteNumbersNumberIdProfilesId($number_id, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NumbersApi->deleteNumbersNumberIdProfilesId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **number_id** | **int**| Number ID | |
| **id** | **int**| Profile ID | |

### Return type

[**\OpenAPI\Client\Model\Profile**](../Model/Profile.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getNumbers()`

```php
getNumbers($filter_prefix, $filter_number, $filter_activated, $filter_name, $filter_description, $page, $per_page): \OpenAPI\Client\Model\Number[]
```

Get all numbers

Provides a list of all numbers

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\NumbersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter_prefix = 'filter_prefix_example'; // string
$filter_number = 'filter_number_example'; // string
$filter_activated = True; // bool
$filter_name = 'filter_name_example'; // string
$filter_description = 'filter_description_example'; // string
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getNumbers($filter_prefix, $filter_number, $filter_activated, $filter_name, $filter_description, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NumbersApi->getNumbers: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **filter_prefix** | **string**|  | [optional] |
| **filter_number** | **string**|  | [optional] |
| **filter_activated** | **bool**|  | [optional] |
| **filter_name** | **string**|  | [optional] |
| **filter_description** | **string**|  | [optional] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\Number[]**](../Model/Number.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getNumbersId()`

```php
getNumbersId($id): \OpenAPI\Client\Model\Number
```

Retrieve a number

Fetches a number by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\NumbersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Number ID

try {
    $result = $apiInstance->getNumbersId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NumbersApi->getNumbersId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Number ID | |

### Return type

[**\OpenAPI\Client\Model\Number**](../Model/Number.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getNumbersNumberIdProfiles()`

```php
getNumbersNumberIdProfiles($number_id): \OpenAPI\Client\Model\Profile
```

List profiles for a number

Lists all available profiles for a number

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\NumbersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$number_id = 56; // int | Number ID

try {
    $result = $apiInstance->getNumbersNumberIdProfiles($number_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NumbersApi->getNumbersNumberIdProfiles: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **number_id** | **int**| Number ID | |

### Return type

[**\OpenAPI\Client\Model\Profile**](../Model/Profile.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postNumbersNumberIdProfiles()`

```php
postNumbersNumberIdProfiles($number_id, $numbers_number_id_profiles): \OpenAPI\Client\Model\Profile
```

Creates a new profile for a number

Shows the active profile for a number

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\NumbersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$number_id = 56; // int | Number ID
$numbers_number_id_profiles = new \OpenAPI\Client\Model\PostNumbersNumberIdProfiles(); // \OpenAPI\Client\Model\PostNumbersNumberIdProfiles

try {
    $result = $apiInstance->postNumbersNumberIdProfiles($number_id, $numbers_number_id_profiles);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NumbersApi->postNumbersNumberIdProfiles: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **number_id** | **int**| Number ID | |
| **numbers_number_id_profiles** | [**\OpenAPI\Client\Model\PostNumbersNumberIdProfiles**](../Model/PostNumbersNumberIdProfiles.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Profile**](../Model/Profile.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putNumbersNumberIdProfiles()`

```php
putNumbersNumberIdProfiles($number_id, $numbers_number_id_profiles): \OpenAPI\Client\Model\Profile
```

Activates a profile for a number

Change the active profile for a number

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\NumbersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$number_id = 56; // int | Number ID
$numbers_number_id_profiles = new \OpenAPI\Client\Model\PutNumbersNumberIdProfiles(); // \OpenAPI\Client\Model\PutNumbersNumberIdProfiles

try {
    $result = $apiInstance->putNumbersNumberIdProfiles($number_id, $numbers_number_id_profiles);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling NumbersApi->putNumbersNumberIdProfiles: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **number_id** | **int**| Number ID | |
| **numbers_number_id_profiles** | [**\OpenAPI\Client\Model\PutNumbersNumberIdProfiles**](../Model/PutNumbersNumberIdProfiles.md)|  | |

### Return type

[**\OpenAPI\Client\Model\Profile**](../Model/Profile.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
