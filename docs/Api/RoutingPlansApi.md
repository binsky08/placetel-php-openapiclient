# OpenAPI\Client\RoutingPlansApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteRoutingPlansId()**](RoutingPlansApi.md#deleteRoutingPlansId) | **DELETE** /routing_plans/{id} | Delete a routing plan |
| [**getRoutingPlans()**](RoutingPlansApi.md#getRoutingPlans) | **GET** /routing_plans | Get all routing plans |
| [**getRoutingPlansId()**](RoutingPlansApi.md#getRoutingPlansId) | **GET** /routing_plans/{id} | Get a routing plan |
| [**getRoutingPlansIdSimulate()**](RoutingPlansApi.md#getRoutingPlansIdSimulate) | **GET** /routing_plans/{id}/simulate | Simulate the usage of a routing plan |
| [**postRoutingPlans()**](RoutingPlansApi.md#postRoutingPlans) | **POST** /routing_plans | Create a routing plan |
| [**putRoutingPlansId()**](RoutingPlansApi.md#putRoutingPlansId) | **PUT** /routing_plans/{id} | Update a routing plan |


## `deleteRoutingPlansId()`

```php
deleteRoutingPlansId($id): \OpenAPI\Client\Model\RoutingPlan
```

Delete a routing plan

Delete a routing plan

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingPlansApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int

try {
    $result = $apiInstance->deleteRoutingPlansId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingPlansApi->deleteRoutingPlansId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |

### Return type

[**\OpenAPI\Client\Model\RoutingPlan**](../Model/RoutingPlan.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getRoutingPlans()`

```php
getRoutingPlans($page, $per_page): \OpenAPI\Client\Model\RoutingPlan[]
```

Get all routing plans

Provides a list of routing plans

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingPlansApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getRoutingPlans($page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingPlansApi->getRoutingPlans: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\RoutingPlan[]**](../Model/RoutingPlan.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getRoutingPlansId()`

```php
getRoutingPlansId($id): \OpenAPI\Client\Model\RoutingPlan
```

Get a routing plan

Get a routing plans

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingPlansApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | ID of a routing plan

try {
    $result = $apiInstance->getRoutingPlansId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingPlansApi->getRoutingPlansId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **string**| ID of a routing plan | |

### Return type

[**\OpenAPI\Client\Model\RoutingPlan**](../Model/RoutingPlan.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getRoutingPlansIdSimulate()`

```php
getRoutingPlansIdSimulate($id, $time): \OpenAPI\Client\Model\RoutingPlan
```

Simulate the usage of a routing plan

Simulate the usage of a routing plan

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingPlansApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | The ID of the routing plan
$time = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | The time at which the simulated call would happen

try {
    $result = $apiInstance->getRoutingPlansIdSimulate($id, $time);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingPlansApi->getRoutingPlansIdSimulate: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **string**| The ID of the routing plan | |
| **time** | **\DateTime**| The time at which the simulated call would happen | [optional] |

### Return type

[**\OpenAPI\Client\Model\RoutingPlan**](../Model/RoutingPlan.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postRoutingPlans()`

```php
postRoutingPlans($routing_plans): \OpenAPI\Client\Model\RoutingPlan
```

Create a routing plan

Create a routing plan

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingPlansApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$routing_plans = new \OpenAPI\Client\Model\PostRoutingPlans(); // \OpenAPI\Client\Model\PostRoutingPlans

try {
    $result = $apiInstance->postRoutingPlans($routing_plans);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingPlansApi->postRoutingPlans: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **routing_plans** | [**\OpenAPI\Client\Model\PostRoutingPlans**](../Model/PostRoutingPlans.md)|  | |

### Return type

[**\OpenAPI\Client\Model\RoutingPlan**](../Model/RoutingPlan.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putRoutingPlansId()`

```php
putRoutingPlansId($id, $routing_plans): \OpenAPI\Client\Model\RoutingPlan
```

Update a routing plan

Update a routing plan

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingPlansApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int
$routing_plans = new \OpenAPI\Client\Model\PutRoutingPlans(); // \OpenAPI\Client\Model\PutRoutingPlans

try {
    $result = $apiInstance->putRoutingPlansId($id, $routing_plans);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingPlansApi->putRoutingPlansId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**|  | |
| **routing_plans** | [**\OpenAPI\Client\Model\PutRoutingPlans**](../Model/PutRoutingPlans.md)|  | |

### Return type

[**\OpenAPI\Client\Model\RoutingPlan**](../Model/RoutingPlan.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
