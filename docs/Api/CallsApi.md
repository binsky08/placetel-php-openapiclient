# OpenAPI\Client\CallsApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteCallsId()**](CallsApi.md#deleteCallsId) | **DELETE** /calls/{id} | Delete a call |
| [**getCalls()**](CallsApi.md#getCalls) | **GET** /calls | Fetch all incoming calls |
| [**getCallsId()**](CallsApi.md#getCallsId) | **GET** /calls/{id} | Retrieve a call |
| [**postCalls()**](CallsApi.md#postCalls) | **POST** /calls | Initate a call |


## `deleteCallsId()`

```php
deleteCallsId($id)
```

Delete a call

Delete a call by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Call ID

try {
    $apiInstance->deleteCallsId($id);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->deleteCallsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Call ID | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getCalls()`

```php
getCalls($filter_date, $filter_from_number, $filter_to_number, $filter_type, $order, $page, $per_page): \OpenAPI\Client\Model\Call[]
```

Fetch all incoming calls

Provides a list of all calls on a specific day (`date`) or today.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | YYYY-MM-DD, defaults to today.
$filter_from_number = 'filter_from_number_example'; // string
$filter_to_number = 'filter_to_number_example'; // string
$filter_type = 'filter_type_example'; // string
$order = 'desc'; // string
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getCalls($filter_date, $filter_from_number, $filter_to_number, $filter_type, $order, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->getCalls: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **filter_date** | **\DateTime**| YYYY-MM-DD, defaults to today. | [optional] |
| **filter_from_number** | **string**|  | [optional] |
| **filter_to_number** | **string**|  | [optional] |
| **filter_type** | **string**|  | [optional] |
| **order** | **string**|  | [optional] [default to &#39;desc&#39;] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\Call[]**](../Model/Call.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getCallsId()`

```php
getCallsId($id): \OpenAPI\Client\Model\Call
```

Retrieve a call

Fetches a call by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Call ID

try {
    $result = $apiInstance->getCallsId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->getCallsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Call ID | |

### Return type

[**\OpenAPI\Client\Model\Call**](../Model/Call.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postCalls()`

```php
postCalls($calls)
```

Initate a call

Initates a call

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\CallsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$calls = new \OpenAPI\Client\Model\PostCalls(); // \OpenAPI\Client\Model\PostCalls

try {
    $apiInstance->postCalls($calls);
} catch (Exception $e) {
    echo 'Exception when calling CallsApi->postCalls: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **calls** | [**\OpenAPI\Client\Model\PostCalls**](../Model/PostCalls.md)|  | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
