# OpenAPI\Client\RecordingsApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteRecordingsId()**](RecordingsApi.md#deleteRecordingsId) | **DELETE** /recordings/{id} | Delete a recording |
| [**getRecordings()**](RecordingsApi.md#getRecordings) | **GET** /recordings | Fetch call recordings |
| [**getRecordingsId()**](RecordingsApi.md#getRecordingsId) | **GET** /recordings/{id} | Fetch specific recording |


## `deleteRecordingsId()`

```php
deleteRecordingsId($id)
```

Delete a recording

Delete a recording by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RecordingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Recording ID

try {
    $apiInstance->deleteRecordingsId($id);
} catch (Exception $e) {
    echo 'Exception when calling RecordingsApi->deleteRecordingsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Recording ID | |

### Return type

void (empty response body)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getRecordings()`

```php
getRecordings($filter_direction, $filter_from, $filter_to, $filter_date, $order, $page, $per_page): \OpenAPI\Client\Model\Recording[]
```

Fetch call recordings

Provides a list of all call recordings

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RecordingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter_direction = array('filter_direction_example'); // string[]
$filter_from = 'filter_from_example'; // string
$filter_to = 'filter_to_example'; // string
$filter_date = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime | YYYY-MM-DD
$order = 'desc'; // string
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getRecordings($filter_direction, $filter_from, $filter_to, $filter_date, $order, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecordingsApi->getRecordings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **filter_direction** | [**string[]**](../Model/string.md)|  | [optional] |
| **filter_from** | **string**|  | [optional] |
| **filter_to** | **string**|  | [optional] |
| **filter_date** | **\DateTime**| YYYY-MM-DD | [optional] |
| **order** | **string**|  | [optional] [default to &#39;desc&#39;] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\Recording[]**](../Model/Recording.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getRecordingsId()`

```php
getRecordingsId($id): \OpenAPI\Client\Model\Recording
```

Fetch specific recording

Fetch recording

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RecordingsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Recording ID

try {
    $result = $apiInstance->getRecordingsId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RecordingsApi->getRecordingsId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Recording ID | |

### Return type

[**\OpenAPI\Client\Model\Recording**](../Model/Recording.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
