# OpenAPI\Client\RoutingPlanObjectsApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**getRoutingPlansIdRoutings()**](RoutingPlanObjectsApi.md#getRoutingPlansIdRoutings) | **GET** /routing_plans/{id}/routings | Get a routing plans objects |


## `getRoutingPlansIdRoutings()`

```php
getRoutingPlansIdRoutings($id): \OpenAPI\Client\Model\RoutingObject[]
```

Get a routing plans objects

Get objects for routing plan

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoutingPlanObjectsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 'id_example'; // string | ID of a routing plan

try {
    $result = $apiInstance->getRoutingPlansIdRoutings($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoutingPlanObjectsApi->getRoutingPlansIdRoutings: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **string**| ID of a routing plan | |

### Return type

[**\OpenAPI\Client\Model\RoutingObject[]**](../Model/RoutingObject.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
