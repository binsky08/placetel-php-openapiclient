# OpenAPI\Client\SipUsersApi

All URIs are relative to https://api.placetel.de/v2, except if the operation defines another base path.

| Method | HTTP request | Description |
| ------------- | ------------- | ------------- |
| [**deleteSipUsersId()**](SipUsersApi.md#deleteSipUsersId) | **DELETE** /sip_users/{id} | Delete a sip user |
| [**deleteSipUsersIdShortCodesCodeId()**](SipUsersApi.md#deleteSipUsersIdShortCodesCodeId) | **DELETE** /sip_users/{id}/short_codes/{code_id} | Delete short code |
| [**getSipUsers()**](SipUsersApi.md#getSipUsers) | **GET** /sip_users | Fetch all sip users |
| [**getSipUsersId()**](SipUsersApi.md#getSipUsersId) | **GET** /sip_users/{id} | Fetch a sip user |
| [**getSipUsersIdShortCodes()**](SipUsersApi.md#getSipUsersIdShortCodes) | **GET** /sip_users/{id}/short_codes | Short codes |
| [**postSipUsers()**](SipUsersApi.md#postSipUsers) | **POST** /sip_users | Create a sip user |
| [**postSipUsersIdShortCodes()**](SipUsersApi.md#postSipUsersIdShortCodes) | **POST** /sip_users/{id}/short_codes | Create short code |
| [**putSipUsersId()**](SipUsersApi.md#putSipUsersId) | **PUT** /sip_users/{id} | Update a sip user |


## `deleteSipUsersId()`

```php
deleteSipUsersId($id): \OpenAPI\Client\Model\SipUser
```

Delete a sip user

Delete a sip user by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SipUsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Sip user ID

try {
    $result = $apiInstance->deleteSipUsersId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SipUsersApi->deleteSipUsersId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Sip user ID | |

### Return type

[**\OpenAPI\Client\Model\SipUser**](../Model/SipUser.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteSipUsersIdShortCodesCodeId()`

```php
deleteSipUsersIdShortCodesCodeId($id, $code_id): \OpenAPI\Client\Model\ShortCode
```

Delete short code

Delete sip user short code

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SipUsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Sip user ID
$code_id = 56; // int | Short code ID

try {
    $result = $apiInstance->deleteSipUsersIdShortCodesCodeId($id, $code_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SipUsersApi->deleteSipUsersIdShortCodesCodeId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Sip user ID | |
| **code_id** | **int**| Short code ID | |

### Return type

[**\OpenAPI\Client\Model\ShortCode**](../Model/ShortCode.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getSipUsers()`

```php
getSipUsers($filter_did, $filter_username, $filter_name, $filter_description, $page, $per_page): \OpenAPI\Client\Model\SipUser[]
```

Fetch all sip users

Fetch a list of all sip users

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SipUsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter_did = 56; // int
$filter_username = 'filter_username_example'; // string
$filter_name = 'filter_name_example'; // string
$filter_description = 'filter_description_example'; // string
$page = 1; // int | Page of results to fetch.
$per_page = 25; // int | Number of results to return per page.

try {
    $result = $apiInstance->getSipUsers($filter_did, $filter_username, $filter_name, $filter_description, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SipUsersApi->getSipUsers: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **filter_did** | **int**|  | [optional] |
| **filter_username** | **string**|  | [optional] |
| **filter_name** | **string**|  | [optional] |
| **filter_description** | **string**|  | [optional] |
| **page** | **int**| Page of results to fetch. | [optional] [default to 1] |
| **per_page** | **int**| Number of results to return per page. | [optional] [default to 25] |

### Return type

[**\OpenAPI\Client\Model\SipUser[]**](../Model/SipUser.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getSipUsersId()`

```php
getSipUsersId($id): \OpenAPI\Client\Model\SipUser
```

Fetch a sip user

Fetch a sip users by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SipUsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Sip user ID

try {
    $result = $apiInstance->getSipUsersId($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SipUsersApi->getSipUsersId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Sip user ID | |

### Return type

[**\OpenAPI\Client\Model\SipUser**](../Model/SipUser.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getSipUsersIdShortCodes()`

```php
getSipUsersIdShortCodes($id): \OpenAPI\Client\Model\ShortCode
```

Short codes

List sip user short codes

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SipUsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Sip user ID

try {
    $result = $apiInstance->getSipUsersIdShortCodes($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SipUsersApi->getSipUsersIdShortCodes: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Sip user ID | |

### Return type

[**\OpenAPI\Client\Model\ShortCode**](../Model/ShortCode.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postSipUsers()`

```php
postSipUsers($sip_users): \OpenAPI\Client\Model\SipUser
```

Create a sip user

Create a new sip user

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SipUsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sip_users = new \OpenAPI\Client\Model\PostSipUsers(); // \OpenAPI\Client\Model\PostSipUsers

try {
    $result = $apiInstance->postSipUsers($sip_users);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SipUsersApi->postSipUsers: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **sip_users** | [**\OpenAPI\Client\Model\PostSipUsers**](../Model/PostSipUsers.md)|  | |

### Return type

[**\OpenAPI\Client\Model\SipUser**](../Model/SipUser.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `postSipUsersIdShortCodes()`

```php
postSipUsersIdShortCodes($id, $sip_users_id_short_codes): \OpenAPI\Client\Model\ShortCode
```

Create short code

Create sip user short code

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SipUsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Sip user ID
$sip_users_id_short_codes = new \OpenAPI\Client\Model\PostSipUsersIdShortCodes(); // \OpenAPI\Client\Model\PostSipUsersIdShortCodes

try {
    $result = $apiInstance->postSipUsersIdShortCodes($id, $sip_users_id_short_codes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SipUsersApi->postSipUsersIdShortCodes: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Sip user ID | |
| **sip_users_id_short_codes** | [**\OpenAPI\Client\Model\PostSipUsersIdShortCodes**](../Model/PostSipUsersIdShortCodes.md)|  | |

### Return type

[**\OpenAPI\Client\Model\ShortCode**](../Model/ShortCode.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `putSipUsersId()`

```php
putSipUsersId($id, $sip_users): \OpenAPI\Client\Model\SipUser
```

Update a sip user

Update a sip user by its ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: Bearer
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('Authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('Authorization', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\SipUsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = 56; // int | Sip user ID
$sip_users = new \OpenAPI\Client\Model\PutSipUsers(); // \OpenAPI\Client\Model\PutSipUsers

try {
    $result = $apiInstance->putSipUsersId($id, $sip_users);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SipUsersApi->putSipUsersId: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

| Name | Type | Description  | Notes |
| ------------- | ------------- | ------------- | ------------- |
| **id** | **int**| Sip user ID | |
| **sip_users** | [**\OpenAPI\Client\Model\PutSipUsers**](../Model/PutSipUsers.md)|  | |

### Return type

[**\OpenAPI\Client\Model\SipUser**](../Model/SipUser.md)

### Authorization

[Bearer](../../README.md#Bearer)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
